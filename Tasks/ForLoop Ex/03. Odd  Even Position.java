package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());

        double oddSum = 0;
        double oddMin = 1000000000000000000000000000000000000.00;
        double oddMax = -1000000000000000000000000000000000000.00;

        double evenSum = 0;
        double evenMin = 1000000000000000000000000000000000000.00;
        double evenMax = -1000000000000000000000000000000000000.00;

        for (int i = 1; i <= n; i++) {
            double num = Double.parseDouble(scanner.nextLine());

            if (i % 2 == 0) {
                evenSum += num;
                if (num < evenMin) {
                    evenMin = num;
                }
                if (num > evenMax) {
                    evenMax = num;
                }

            }
            else {
                oddSum += num;
                if (num < oddMin) {
                    oddMin = num;
                }
                if (num > oddMax) {
                    oddMax = num;
                }

            }
        }

        if (n == 1) {
            System.out.printf("OddSum=%.2f,%nOddMin=%.2f,%nOddMax=%.2f,%nEvenSum=%.2f,%nEvenMin=No,%nEvenMax=No",oddSum,oddMin,
                    oddMax,evenSum);
        } else if (n == 0) {
            System.out.printf("OddSum=%.2f,%nOddMin=No,%nOddMax=No,%nEvenSum=%.2f,%nEvenMin=No,%nEvenMax=No",oddSum,evenSum);
        } else {
            System.out.printf("OddSum=%.2f,%nOddMin=%.2f,%nOddMax=%.2f,%nEvenSum=%.2f,%nEvenMin=%.2f,%nEvenMax=%.2f", oddSum, oddMin,
                    oddMax, evenSum, evenMin, evenMax);
        }


    }
}