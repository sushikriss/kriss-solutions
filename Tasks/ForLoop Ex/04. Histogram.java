package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        double countFirst = 0;
        double countSecond = 0;
        double countThird = 0;
        double countFourth = 0;
        double countFive = 0;


        for (int i = 1; i <= n; i++) {
            int num = Integer.parseInt(scanner.nextLine());

            if (num < 200) {
                countFirst++;
            }
            if (num >= 200 && num <= 399) {
                countSecond++;
            }
            if (num >= 400 && num <= 599) {
                countThird++;
            }
            if (num >= 600 && num <= 799) {
                countFourth++;
            }
            if (num >= 800) {
                countFive++;
            }
        }

        double percentOne = countFirst / n * 100;
        double percentTwo = countSecond / n * 100;
        double percentThree = countThird / n * 100;
        double percentFour = countFourth / n * 100;
        double percentFive = countFive / n * 100;

        System.out.printf("%.2f%%%n%.2f%%%n%.2f%%%n%.2f%%%n%.2f%%",percentOne,percentTwo,percentThree,percentFour,percentFive);

    }
}