package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        double exTwo = 0;
        double exThree = 0;
        double exFour = 0;


        for (int i = 1; i <= n; i++) {
            int num = Integer.parseInt(scanner.nextLine());

            if (num % 2 == 0) {
                exTwo++;
            }
            if (num % 3 == 0) {
                exThree++;
            }
            if (num % 4 == 0) {
                exFour++;
            }
        }

        double percentTwo = exTwo / n * 100;
        double percentThree = exThree / n * 100;
        double percentFour = exFour / n * 100;

        System.out.printf("%.2f%%%n%.2f%%%n%.2f%%",percentTwo,percentThree,percentFour);

    }
}