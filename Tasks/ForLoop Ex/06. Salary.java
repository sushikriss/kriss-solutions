package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int tabs = Integer.parseInt(scanner.nextLine());
        int paycheck = Integer.parseInt(scanner.nextLine());

        for (int i = 1; i <= tabs; i++) {
            String website = scanner.nextLine();

            if (website.equals("Facebook")) {
                paycheck -= 150;
            }
            if (website.equals("Instagram")) {
                paycheck -= 100;
            }
            if (website.equals("Reddit")) {
                paycheck -= 50;
            }

            if (paycheck <= 0) {
                break;
            }

        }

        if (paycheck <= 0) {
            System.out.println("You have lost your salary.");
        } else {
            System.out.printf("%d",paycheck);
        }

    }
}