package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numCount = Integer.parseInt(scanner.nextLine());
        int total = 0;
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < numCount; i++) {
            int number = Integer.parseInt(scanner.nextLine());
            total += number;

            if (number > max) {
                max = number;
            }


        }

        if (max == (total - max)) {
            System.out.printf("Yes%nSum = %d",max);
        } else {
            System.out.printf("No%nDiff = %d",Math.abs(max-(total-max)));
        }

    }
}