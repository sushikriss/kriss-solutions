package com.company;
import java.util.Scanner;
public class Comissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int evenFloors = Integer.parseInt(scanner.nextLine());
        int roomsPerFloor = Integer.parseInt(scanner.nextLine());

        for (int floor = evenFloors; floor >= 1; floor--) {
            for (int ap = 0; ap < roomsPerFloor; ap++) {
                if (floor == evenFloors) {
                    System.out.print("L" + floor + ap + " ");
                } else if (floor % 2 == 0) {
                    System.out.print("O" + floor + ap + " ");
                } else {
                    System.out.print("A" + floor + ap + " ");
                }
            }
            System.out.println();
        }




    }
}
