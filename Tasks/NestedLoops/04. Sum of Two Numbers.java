package com.company;
import java.util.Scanner;
public class Comissions { // 15:15
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numStart = Integer.parseInt(scanner.nextLine());
        int numEnd = Integer.parseInt(scanner.nextLine());
        int magicNum = Integer.parseInt(scanner.nextLine());
        int count = 0;
        boolean found = false;

        for (int i = numStart; i <= numEnd; i++) {
            for (int a = numStart; a <= numEnd; a++) {
                count++;
                if (i + a == magicNum) {
                    found = true;
                    System.out.printf("Combination N:%d (%d + %d = %d)",count,i,a,magicNum);
                    break;
                }
            }
            if (found) {
                break;
            }
        }

        if (found) {
        } else {
            System.out.printf("%d combinations - neither equals %d",count,magicNum);
        }

    }
}
