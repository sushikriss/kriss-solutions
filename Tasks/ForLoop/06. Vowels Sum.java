package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String word = scanner.nextLine();
        int result = 0;

        for (int hash = 0; hash < word.length(); hash++) {
            char symbol = word.charAt(hash);

            switch (symbol) {
                case 'a':
                    result += 1;
                    break;
                case 'e':
                    result += 2;
                    break;
                case 'i':
                    result += 3;
                    break;
                case 'o':
                    result += 4;
                    break;
                case 'u':
                    result += 5;
                    break;
            }
        }

        System.out.println(result);

    }
}