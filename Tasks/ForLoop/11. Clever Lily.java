package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int lilyAge = Integer.parseInt(scanner.nextLine());
        double washingMachinePrice = Double.parseDouble(scanner.nextLine());
        double toyPrice = Double.parseDouble(scanner.nextLine());
        int moneyCollected = 0;
        int toyCount = 0;
        double money = 0;

        for (int i = 1; i <= lilyAge; i++) {
            if (i % 2 == 0) {
                moneyCollected += 10;
                money += moneyCollected - 1;
            }
            else {
                toyCount++;
            }
        }

        double toyMoney = toyCount * toyPrice;
        double spend = toyMoney + money;

        if ((spend >= washingMachinePrice)) {
            System.out.printf("Yes! %.2f",spend-washingMachinePrice);
        } else {
            System.out.printf("No! %.2f",washingMachinePrice-spend);
        }



    }
}