package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num = Integer.parseInt(scanner.nextLine());
        int totalFirst = 0;
        int totalSecond = 0;

        for (int i = 0; i < num * 2; i++) {
            int numa = Integer.parseInt(scanner.nextLine());

            if (i < num) {
                totalFirst = totalFirst + numa;
            } else {
                totalSecond = totalSecond + numa;
            }
        }

        if (totalFirst == totalSecond) {
            System.out.printf("Yes, sum = %d",totalFirst);
        }

        if (totalSecond != totalFirst) {
            System.out.printf("No, diff = %d",Math.abs(totalFirst-totalSecond));
        }

    }
}