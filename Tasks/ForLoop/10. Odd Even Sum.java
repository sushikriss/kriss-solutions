package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        int totalChetno = 0;
        int totalNeChetno = 0;

        for (int i = 0; i < n; i++) {
            int num = Integer.parseInt(scanner.nextLine());
            if (i % 2 == 0) {
                totalChetno += num;
            } else {
                totalNeChetno += num;
            }
        }

        if (totalChetno==totalNeChetno) {
            System.out.printf("Yes%nSum = %d",totalChetno);
        }
        if (totalChetno!=totalNeChetno) {
            System.out.printf("No%nDiff = %d",Math.abs(totalChetno-totalNeChetno));
        }

    }
}