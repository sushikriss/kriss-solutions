package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());

        for (int zero = 0; zero < n; n--) {
            System.out.println(n);
        }


    }
}