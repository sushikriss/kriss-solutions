package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number = Integer.parseInt(scanner.nextLine());
        int biggest = Integer.MIN_VALUE;
        int smallest = Integer.MAX_VALUE;

        for (int i = 0; i < number; i++) {
            int count = Integer.parseInt(scanner.nextLine());
            if (count > biggest) {
                biggest = count;
            }
            if (count < smallest) {
                smallest = count;
            }
        }

        System.out.printf("Max number: %d%n",biggest);
        System.out.printf("Min number: %d",smallest);

    }
}