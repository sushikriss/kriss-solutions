package com.company;
import java.util.Scanner;
public class Comissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double realCount = 0;
        double allGrates = 0;
        int jury = Integer.parseInt(scanner.nextLine());

        for (int i = 0; i < jury; i++) {
            String presentation = scanner.nextLine();

            while (!presentation.equals("Finish")) {

                double current = 0;
                double count = 0;

                for (int grate = jury; grate > 0; grate--) {
                    double grated = Double.parseDouble(scanner.nextLine());
                    count++;
                    realCount++;

                    allGrates += grated;
                    current += grated;
                }
                System.out.printf("%s - %.2f.%n", presentation, current / count);
                presentation = scanner.nextLine();
            }

            if (presentation.equals("Finish")) {
                break;
            }
        }

        System.out.printf("Student's final assessment is %.2f.",allGrates/realCount);


    }
}
