package com.company;
import java.util.Scanner;
public class Comissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numOne = Integer.parseInt(scanner.nextLine());
        int numTwo = Integer.parseInt(scanner.nextLine());

       for (int i = numOne; i < numTwo; i++) {
           String n1 = Integer.toString(i);

           int sumOne = 0;
           int sumTwo = 0;

           for (int a = 0; a < n1.length(); a++) {

               char sym = n1.charAt(a);

               if (a % 2 == 0) {
                    sumOne += sym;
               }
               else {
                   sumTwo += sym;
               }
           }
           if (sumOne == sumTwo) {
               System.out.print(i + " ");
           }
       }

    }
}
