package com.company;
import java.util.Scanner;
public class Comissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int prime = 0;
        int noPrime = 0;
       
        String command = scanner.nextLine();
        while (!command.equals("stop")) {
            int num = Integer.parseInt(command);
            int count = 0;
            
            if (num < 0) {
                System.out.println("Number is negative.");
            } else {
                for (int i = 1; i <= num; i++) {
                    if (num % i == 0) {
                        count++;
                    }
                }
                if (count == 2) {
                    prime += num;
                } else {
                    noPrime += num;
                }
            }

            command = scanner.nextLine();
        }

        System.out.printf("Sum of all prime numbers is: %d%n",prime);
        System.out.printf("Sum of all non prime numbers is: %d",noPrime);
    }
}
