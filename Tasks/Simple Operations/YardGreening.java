package com.company;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Double Metri = Double.parseDouble(scanner.nextLine());
        Double TotalPrice = Metri * 7.61;
        Double Discount = TotalPrice * 0.18;
        Double Final = TotalPrice - Discount;
        System.out.printf("The final price is: %.2f lv.", Final);
        System.out.println();
        System.out.printf("The discount is: %.2f lv.", Discount);

    }
}
