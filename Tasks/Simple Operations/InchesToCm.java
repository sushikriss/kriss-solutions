package com.company;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Double Num = Double.parseDouble(scanner.nextLine());
        Double Area = Num * 2.54;
        System.out.printf("%.2f", Area);
    }
}
