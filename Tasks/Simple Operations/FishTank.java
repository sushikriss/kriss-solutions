package com.company;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int Duljina = Integer.parseInt(scanner.nextLine());
        int Shirok = Integer.parseInt(scanner.nextLine());
        int Visok = Integer.parseInt(scanner.nextLine());
        double Procent = Double.parseDouble(scanner.nextLine());

        int Obem = Duljina * Shirok * Visok;
        double Total = Obem * 0.001;
        double Percent = Procent*0.01;
        double Real = Total*(1-Percent);
        System.out.printf("%.3f", Real);
    }
}
