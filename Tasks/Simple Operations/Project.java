package com.company;

import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String Name = scanner.nextLine();
        int NumA = Integer.parseInt(scanner.nextLine());
        int Result = NumA * 3;
        System.out.printf("The architect %s " +
                "will need %d hours to complete %d project/s.",Name,Result,NumA);
    }
}
