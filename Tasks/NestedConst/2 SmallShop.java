package com.company;
import java.util.Scanner;

public class ConsoleConverterPlus {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String food = scanner.nextLine().toLowerCase();
        String city = scanner.nextLine().toLowerCase();
        double cena = Double.parseDouble(scanner.nextLine());

        switch (food) {
            case "peanuts": {
                if (city.equals("sofia")) {
                    cena = cena * 1.60; break;
                }
                else if (city.equals("plovdiv")) {
                    cena = cena * 1.50; break;
                }
                else {
                    cena = cena * 1.55; break;
                }
            }
            case "sweets": {
                if (city.equals("sofia")) {
                    cena = cena * 1.45; break;
                }
                else if (city.equals("plovdiv")) {
                    cena = cena * 1.30; break;
                }
                else {
                    cena = cena * 1.35; break;
                }
            }
            case "beer": {
                if (city.equals("sofia")) {
                    cena = cena * 1.20; break;
                }
                else if (city.equals("plovdiv")) {
                    cena = cena * 1.15; break;
                }
                else {
                    cena = cena * 1.10; break;
                }
            }
            case "water": {
                if (city.equals("sofia")) {
                    cena = cena * 0.80; break;
                }
                else if (city.equals("plovdiv")) {
                    cena = cena * 0.70; break;
                }
                else {
                    cena = cena * 0.70; break;
                }
            }
            case "coffee": {
                if (city.equals("sofia")) {
                    cena = cena * 0.50; break;
                }
                else if (city.equals("plovdiv")) {
                    cena = cena * 0.40; break;
                }
                else {
                    cena = cena * 0.45; break;
                }
            }
        }
        System.out.println(cena);


    }
}
