package com.company;

import java.util.Scanner;

public class FishTank {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String town = scanner.nextLine();
        double obem = Double.parseDouble(scanner.nextLine());
        double comission = 0;

        switch (town) {
            case "Plovdiv": {
                if (obem >= 0 && obem <= 500) {
                    comission = 0.055; break;
                } else if (obem > 500 && obem <= 1000) {
                    comission = 0.08; break;
                } else if (obem > 1000 && obem <= 10000) {
                    comission = 0.12; break;
                } else if (obem > 10000) {
                    comission = 0.145; break;
                }
            }
            case "Varna": {
                if (obem >= 0 && obem <= 500) {
                    comission = 0.045; break;
                } else if (obem > 500 && obem <= 1000) {
                    comission = 0.075; break;
                } else if (obem > 1000 && obem <= 10000) {
                    comission = 0.10; break;
                } else if (obem > 10000) {
                    comission = 0.13; break;
                }
            }
            case "Sofia": {
                if (obem >= 0 && obem <= 500) {
                    comission = 0.05; break;
                } else if (obem > 500 && obem <= 1000) {
                    comission = 0.07; break;
                } else if (obem > 1000 && obem <= 10000) {
                    comission = 0.08; break;
                } else if (obem > 10000) {
                    comission = 0.12; break;
                }
            }
        }
        if (comission != 0) {
            double go = comission * obem;
            System.out.printf("%.2f",go);
        } else {
            System.out.println("error");
        }




    }
}
