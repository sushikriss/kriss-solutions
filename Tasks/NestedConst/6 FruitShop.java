package com.company;

import java.util.Scanner;

public class FishTank {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String fruit = scanner.nextLine();
        String day = scanner.nextLine();
        double kg = Double.parseDouble(scanner.nextLine());
        double paco = 0;

        boolean Work = (day.equals("Monday")||day.equals("Tuesday")||day.equals("Wednesday")||day.equals("Thursday")||day.equals("Friday"));
        boolean Chill = (day.equals("Saturday")||day.equals("Sunday"));

        if (Work) {
            switch (fruit) {
                case "banana":
                    paco = kg * 2.50; break;
                case "apple":
                    paco = kg * 1.20; break;
                case "orange":
                    paco = kg * 0.85; break;
                case "grapefruit":
                    paco = kg * 1.45; break;
                case "kiwi":
                    paco = kg * 2.70; break;
                case "pineapple":
                    paco = kg * 5.50; break;
                case "grapes":
                    paco = kg * 3.85; break;
            }
        } else if (Chill) {
            switch (fruit) {
                case "banana":
                    paco = kg * 2.70;
                    break;
                case "apple":
                    paco = kg * 1.25;
                    break;
                case "orange":
                    paco = kg * 0.90;
                    break;
                case "grapefruit":
                    paco = kg * 1.60;
                    break;
                case "kiwi":
                    paco = kg * 3.00;
                    break;
                case "pineapple":
                    paco = kg * 5.60;
                    break;
                case "grapes":
                    paco = kg * 4.20;
                    break;
            }
        }
        if (paco != 0) {
            System.out.printf("%.2f",paco);
        }
        else System.out.println("error");







    }
}
