package com.company;

import java.util.Scanner;

public class ConsoleConverterPlus {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double Dohod = Double.parseDouble(scanner.nextLine());
        double Uspeh = Double.parseDouble(scanner.nextLine());
        double RabotnaZap = Double.parseDouble(scanner.nextLine());
        double Social = RabotnaZap * 0.35;
        double BigStipendia = Uspeh*25;

        if (Dohod >= RabotnaZap) {
            if (Uspeh >= 5.5) {
                System.out.printf("You get a scholarship for excellent results %.0f BGN",Math.floor(BigStipendia));
            }
            else if (Uspeh < 5.5) {
                System.out.printf("You cannot get a scholarship!");
            }
        }
        else if (Dohod < RabotnaZap) {
            if (Uspeh > 4.5 && Uspeh < 5.5) {
                System.out.printf("You get a Social scholarship %.0f BGN",Math.floor(Social));
            }
            else if (BigStipendia > Social && Uspeh >= 5.5) {
                System.out.printf("You get a scholarship for excellent results %.0f BGN",Math.floor(BigStipendia));
            }
            else if (Uspeh >= 5.5 && BigStipendia < Social) {
                System.out.printf("You get a Social scholarship %.0f BGN",Math.floor(Social));
            }
            else {
                System.out.printf("You cannot get a scholarship!");
            }
        }







    }
}
