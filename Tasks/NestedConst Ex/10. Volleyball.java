package com.company;

import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String year = scanner.nextLine(); //leap or normal // visokosna ili nevisokosna
        double numHolidays = Double.parseDouble(scanner.nextLine());
        int travel = Integer.parseInt(scanner.nextLine());
        int play = 0;
        double bonusPlay = 0;
        double TotalPlay = 0;

        if (year.equals("leap")) {
            double weekens = 48;
            double freeweekends = weekens - travel;
            double saturdayGames = freeweekends * 3/4;
            double holidayGames = numHolidays * 2/3;
            TotalPlay = saturdayGames + holidayGames + travel;
            bonusPlay = TotalPlay + (TotalPlay * 0.15);
        } else if (year.equals("normal")) {
            double weekens = 48;
            double freeweekends = weekens - travel;
            double saturdayGames = freeweekends * 3/4;
            double holidayGames = numHolidays * 2/3;
            TotalPlay = saturdayGames + holidayGames + travel;
        }

        if (year.equals("leap")) {
            System.out.println(Math.floor(bonusPlay));
        } else {
            System.out.println(Math.floor(TotalPlay));
        }

    }
}