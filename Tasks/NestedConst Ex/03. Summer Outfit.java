package com.company;

import java.util.Scanner;
public class SwitchVersion {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double Gradus = Double.parseDouble(scanner.nextLine());
        String type = scanner.nextLine();

        if (Gradus>=10&&Gradus<=18) {
            switch (type) {
                case "Morning":
                    System.out.printf("It's %.0f degrees, get your Sweatshirt and Sneakers.",Gradus); break;
                case "Afternoon":
                    System.out.printf("It's %.0f degrees, get your Shirt and Moccasins.",Gradus); break;
                case "Evening":
                    System.out.printf("It's %.0f degrees, get your Shirt and Moccasins.",Gradus); break;
            }
        } else if (Gradus<=24&&Gradus>18) {
            switch (type) {
                case "Morning":
                    System.out.printf("It's %.0f degrees, get your Shirt and Moccasins.",Gradus); break;
                case "Afternoon":
                    System.out.printf("It's %.0f degrees, get your T-Shirt and Sandals.",Gradus); break;
                case "Evening":
                    System.out.printf("It's %.0f degrees, get your Shirt and Moccasins.",Gradus); break;
            }
        }else if (Gradus>=25) {
            switch (type) {
                case "Morning":
                    System.out.printf("It's %.0f degrees, get your T-Shirt and Sandals.",Gradus); break;
                case "Afternoon":
                    System.out.printf("It's %.0f degrees, get your Swim Suit and Barefoot.",Gradus); break;
                case "Evening":
                    System.out.printf("It's %.0f degrees, get your Shirt and Moccasins.",Gradus); break;
            }
        }

    }
}
