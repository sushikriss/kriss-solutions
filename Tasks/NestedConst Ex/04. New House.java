package com.company;

import java.util.Scanner;
public class SwitchVersion {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String type = scanner.nextLine();
        int numa = Integer.parseInt(scanner.nextLine());
        int Bujet = Integer.parseInt(scanner.nextLine());

        double priceR = numa * 5;
        double priceD = numa * 3.80;
        double priceT = numa * 2.80;
        double priceN = numa * 3;
        double priceG = numa * 2.50;
        double end = 0;
        double difference = 0;

        if (type.equals("Roses")) {
            if (numa>80) {
                end = priceR * 0.9;
            } else {
                end = priceR;
            }
        }
        else if (type.equals("Dahlias")) {
            if (numa>90) {
                end = priceD * 0.85;
            } else {
                end = priceD;
            }
        }
        else if (type.equals("Tulips")) {
            if (numa>80) {
                end = priceT * 0.85;
            } else {
                end = priceT;
            }
        }
        else if (type.equals("Narcissus")) {
            if (numa<120) {
                end = priceN +(priceN*0.15);
            } else {
                end = priceN;
            }
        }
        else if (type.equals("Gladiolus")) {
            if (numa<80) {
                end = priceG +(priceG*0.20);
            } else {
                end = priceG;
            }
        }

        difference = Math.abs(Bujet-end);
        if (Bujet>=end) {
            System.out.printf("Hey, you have a great garden with %d %s and %.2f leva left.",numa,type,difference);
        } else {
            System.out.printf("Not enough money, you need %.2f leva more.",difference);
        }

    }
}
