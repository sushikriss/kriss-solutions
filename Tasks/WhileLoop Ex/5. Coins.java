package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double amount = Double.parseDouble(scanner.nextLine());
        double trans = amount * 100;
        int count = 0;

        while (trans >= 200) {
            trans -= 200;
            Math.round(trans);
            count++;
        }

        while (trans >= 100) {
            trans -= 100;
            Math.round(trans);
            count++;
        }

        while (trans >= 50) {
            trans -= 50;
            Math.round(trans);
            count++;
        }

        while (trans >= 20) {
            trans -= 20;
            Math.round(trans);
            count++;
        }

        while (trans >= 10) {
            trans -= 10;
            Math.round(trans);
            count++;
        }

        while (trans >= 5) {
            trans -= 5;
            Math.round(trans);
            count++;
        }

        while (trans >= 2) {
            trans -= 2;
            Math.round(trans);
            count++;
        }

        while (trans >= 1) {
            trans -= 1;
            Math.round(trans);
            count++;
        }

        System.out.println(count);

    }
}