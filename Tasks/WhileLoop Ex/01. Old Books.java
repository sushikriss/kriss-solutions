package com.company;

import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String searchedBook = scanner.nextLine();
        int bookshelfSpace = Integer.parseInt(scanner.nextLine());
        int bookCount = 0;

        while (bookshelfSpace>0) {
            bookshelfSpace--;
            String bookName = scanner.nextLine();
            if (bookName.equals(searchedBook)) {
                System.out.printf("You checked %d books and found it.",bookCount); break;
            }
            bookCount++;
            if (bookshelfSpace==0) {
                System.out.printf("The book you search is not here!%nYou checked %d books.",bookCount); break;
            }
        }


    }
}