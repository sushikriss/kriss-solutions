package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int Width = Integer.parseInt(scanner.nextLine());
        int Long = Integer.parseInt(scanner.nextLine());
        int Cake = Width * Long;
        int totalBites = 0;

        String command = scanner.nextLine();
        while (!command.equals("STOP")) {
            int piece = Integer.parseInt(command);
            totalBites += piece;
            if (totalBites>Cake) {
                Cake = totalBites-Cake;
                System.out.printf("No more cake left! You need %d pieces more.",Cake); break;
            }
            command = scanner.nextLine();
        }

        if (Cake>totalBites) {
            System.out.printf("%d pieces are left.",Cake-totalBites);
        }

    }
}