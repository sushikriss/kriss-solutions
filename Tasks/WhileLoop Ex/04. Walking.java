package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int totalSteps = 0;

        String command = scanner.nextLine();
        while (!command.equals("Going home")) {
            int steps = Integer.parseInt(command);
            totalSteps += steps;
            if (totalSteps>=10000) {
                System.out.println("Goal reached! Good job!"); break;
            }
            command = scanner.nextLine();
        }

        if (command.equals("Going home")) {
            int steps = Integer.parseInt(scanner.nextLine());
            totalSteps += steps;
            if (totalSteps>=10000) {
                System.out.println("Goal reached! Good job!");
            } else {
                System.out.printf("%d more steps to reach goal.",10000-totalSteps);
            }
        }

    }
}