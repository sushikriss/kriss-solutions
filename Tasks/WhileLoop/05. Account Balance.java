package com.company;
import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double numDeposits = Double.parseDouble(scanner.nextLine());
        double total = 0;

        while (numDeposits>0) {
            numDeposits--;
            double value = Double.parseDouble(scanner.nextLine());
            if (value<0) {
                System.out.printf("Invalid operation!%nTotal: %.2f",total); break;
            }
            System.out.printf("Increase: %.2f%n",value);
            total += value;
        }
        if (numDeposits==0) {
            System.out.printf("Total: %.2f",total);
        }

    }
}