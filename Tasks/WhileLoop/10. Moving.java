package com.company;

import java.util.Scanner;
public class WhileLoopINTEGERIMPORTANT {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int Width = Integer.parseInt(scanner.nextLine());
        int Long = Integer.parseInt(scanner.nextLine());
        int High = Integer.parseInt(scanner.nextLine());
        int Volume = Width * Long * High;
        int totalBoxes = 0;

        String dudu = scanner.nextLine();
        while (!dudu.equals("Done")) {
            int box = Integer.parseInt(dudu);
            totalBoxes += box;

            if (totalBoxes>Volume) {
                System.out.printf("No more free space! You need %d Cubic meters more.",totalBoxes-Volume);
                break;
            }
            dudu = scanner.nextLine();


        }

        if (Volume>totalBoxes) {
            System.out.printf("%d Cubic meters left.", Volume - totalBoxes);
        }

    }
}