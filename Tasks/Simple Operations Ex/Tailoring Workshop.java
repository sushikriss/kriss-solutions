package com.company;

import java.util.Scanner;

public class USDtoBGN {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int Masi = Integer.parseInt(scanner.nextLine());
        double LongNaMasaMetri = Double.parseDouble(scanner.nextLine());
        double ShirochinaNaMasaMetri = Double.parseDouble(scanner.nextLine());

        double Pokrivki = Masi * (LongNaMasaMetri+2*0.30)*(ShirochinaNaMasaMetri+2*0.30);
        double Kareta = Masi * (LongNaMasaMetri/2)*(LongNaMasaMetri/2);
        double Cena$ = Pokrivki * 7 + Kareta * 9;
        double CenaLv = Cena$ * 1.85;
        System.out.printf("%.2f USD",Cena$);
        System.out.println();
        System.out.printf("%.2f BGN", CenaLv);

    }
}
