package com.company;

import java.util.Scanner;

public class DanceHall {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double L = Double.parseDouble(scanner.nextLine());
        double W = Double.parseDouble(scanner.nextLine());
        double A = Double.parseDouble(scanner.nextLine());

        double Zala = (L * 100) * (W * 100);
        double Garderob = (A * 100) * (A * 100);
        double Peika = Zala / 10;
        double Prost = Zala - Garderob - Peika;
        double tanciori = Prost / (40+7000);

        System.out.printf("%.0f",Math.floor(tanciori));
    }
}