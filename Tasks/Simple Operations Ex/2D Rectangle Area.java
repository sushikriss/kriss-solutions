package com.company;

import java.util.Scanner;

public class USDtoBGN {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        double x1 = Double.parseDouble(scanner.nextLine());
        double y1 = Double.parseDouble(scanner.nextLine());
        double x2 = Double.parseDouble(scanner.nextLine());
        double y2 = Double.parseDouble(scanner.nextLine());

        double duljina = Math.abs(x1 - x2);
        double shirok = Math.abs(y1 - y2);

        double Plosh = duljina * shirok;
        double Perimeter = 2*(duljina+shirok);
        System.out.printf("%.2f", Plosh);
        System.out.println();
        System.out.printf("%.2f", Perimeter);

    }
}
