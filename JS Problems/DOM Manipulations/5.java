function encodeAndDecodeMessages() {
    let tagElements = Array.from(document.getElementsByTagName('textarea'));
    let messageElement = tagElements[0];
    let encodedMessageElement = tagElements[1];

    let encoding = (e) => {
        if (e.currentTarget.textContent === 'Encode and send it') {
            encodedMessageElement.value = messageElement.value
            .split('')
            .map(e => String.fromCharCode(e.charCodeAt(0) + 1)).join('');
            messageElement.value = "";
        } else {
            encodedMessageElement.value = encodedMessageElement.value
            .split('')
            .map(e => String.fromCharCode(e.charCodeAt(0) - 1)).join('');
        }
    }

    let button = Array.from(document.querySelectorAll('button'));
    button.forEach(x => {
        x.addEventListener('click', encoding);
    });

    
}