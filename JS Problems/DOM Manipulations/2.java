function attachEventsListeners() {
let arr = Array.from(document.querySelectorAll('input[value="Convert"]'));

    const calc = (e) => {
        let allFields = Array.from(document.querySelectorAll('input[type="text"]'));
        if (e.target.matches('#daysBtn')) {
            let days = Number(allFields[0].value);
            allFields[1].value = days * 24; 
            allFields[2].value = days * 1440;
            allFields[3].value = days * 86400;

        } else if (e.target.matches('#hoursBtn')) {
            let hours = Number(allFields[1].value);
            allFields[0].value = hours / 24; 
            allFields[2].value = hours * 60;
            allFields[3].value = hours * 3600;

        } else if (e.target.matches('#minutesBtn')) {
            let minutes = Number(allFields[2].value);
            allFields[0].value = minutes / 1440;            ; 
            allFields[1].value = minutes / 60;
            allFields[3].value = minutes * 60;

        } else {
            let seconds = Number(allFields[3].value);
            allFields[0].value = seconds / 86400; 
            allFields[1].value = seconds / 3600;
            allFields[2].value = seconds / 60;
        }
    };
    arr.forEach(x => {
        x.addEventListener('click', calc);        
    });


}