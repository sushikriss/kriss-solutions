function addItem() {
    let textElement = document.querySelector("#newItemText");
    let valueElement = document.querySelector("#newItemValue");
    let selectedElement = document.getElementById('menu');

    let optionElement = document.createElement('option');
    optionElement.textContent = textElement.value;
    optionElement.value = valueElement.value;
    selectedElement.appendChild(optionElement);

    textElement.value = "";
    valueElement.value = "";


}