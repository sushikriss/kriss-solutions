function create(words) {
   let targetedDiv = document.getElementById('content');
   let arrElement = Array.from(words);

   arrElement.forEach(x => {
      let divElement = document.createElement('div');
      let pElement = document.createElement('p');
      
      pElement.textContent = x;
      pElement.style.display = 'none';
      divElement.appendChild(pElement);
      
      divElement.addEventListener('click', (e) => {
         let p = divElement.childNodes[0];
         p.style.display = 'block';
      });
      
      targetedDiv.appendChild(divElement);
   });

}