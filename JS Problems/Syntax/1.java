function calculate(name, weight, kilos) {
    let result = weight * kilos;
    console.log("I need $" + (result / 1000).toFixed(2) + 
    " to buy " + (weight / 1000).toFixed(2) + " kilograms " + name + ".");
};