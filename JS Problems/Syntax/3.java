function sameNumbers (number) {
    let stringNum = String(number);
    let sum = 0;
    let result = true;

    for (let i = 0; i < stringNum.length; i++) {

        let firstNum = Number(stringNum[i]);
        let secNum = Number(stringNum[i+1]);

        if (firstNum !== secNum && Number.isNaN(secNum) === false) {
            result = false;
        }
        sum += Number(stringNum[i]);
    }

    if (result) {
        console.log(true);
    } else {
        console.log(false);
    }
    console.log(sum);
}