describe('numbers', () => {
    it('checksLengthEven', () => {
        let word = 'even';
        assert.equal(isOddOrEven(word), 'even');
    });

    it('checksLengthOdd', () => {
        let word = 'odd';
        assert.equal(isOddOrEven(word), 'odd');
    });

    it('emptyShouldReturnUndefined', () => {
        let word = '';
        assert.equal(isOddOrEven(word), 'even');
    });

    it('numberShouldReturnUndefined', () => {
        let word = 2;
        assert.strictEqual(isOddOrEven(word), undefined);
    });
});