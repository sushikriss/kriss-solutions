describe('charLookUp', () => {
    it('shouldReturnUndefinedNotString', () => {
        let word = 2;
        let index = 4;
        assert.equal(lookupChar(word, index), undefined);
    });

    it('shouldReturnUndefinedNotNumber', () => {
        let word = 'dang';
        let index = '3';
        assert.equal(lookupChar(word, index), undefined);
    });

    it('invalidIndexWhenIndexLongerThanString', () => {
        let word = 'dang';
        let index = 10;
        assert.equal(lookupChar(word, index), 'Incorrect index');
    });

    it('invalidIndexWhenIndexLessThanZero', () => {
        let word = 'dang';
        let index = -1;
        assert.equal(lookupChar(word, index), 'Incorrect index');
    });

    it('shouldReturnChar', () => {
        let word = 'dang';
        let index = 2;
        assert.equal(lookupChar(word, index), 'n');
    });

    it('onFloatingPointShouldFail', () => {
        let word = 'dang';
        let index = 2.5;
        assert.equal(lookupChar(word, index), undefined);
    });

});