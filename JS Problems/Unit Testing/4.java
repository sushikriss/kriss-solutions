describe('MathEnforcer', () => {
    describe('addFive', () => {
        it('shouldReturnUndefinedIfNotNumber', () => {
            let obj = mathEnforcer.addFive('5');
            assert.equal(obj, undefined);
        });

        it('shouldReturnValidNumber', () => {
            let obj = mathEnforcer.addFive(5);
            assert.equal(obj, 10);
        });

        it('shouldReturnValidNumberWhenFloating', () => {
            let obj = mathEnforcer.addFive(5.5);
            assert.equal(obj, 10.5);
        });

        it('shouldReturnValidNumber', () => {
            let obj = mathEnforcer.addFive(-5);
            assert.equal(obj, 0);
        });
    });

    describe('substractTen', () => {
        it('shouldReturnUndefinedIfNotNumber', () => {
            let obj = mathEnforcer.subtractTen('5');
            assert.equal(obj, undefined);
        });

        it('shouldReturnValidNumber', () => {
            let obj = mathEnforcer.subtractTen(15);
            assert.equal(obj, 5);
        });

        it('shouldReturnValidNumberWhenFloating', () => {
            let obj = mathEnforcer.subtractTen(15.5);
            assert.equal(obj, 5.5);
        });

        it('shouldReturnValidNumber', () => {
            let obj = mathEnforcer.subtractTen(-5);
            assert.equal(obj, -15);
        });
    });

    describe('sum', () => {
        it('shouldReturnUndefinedIfFirstNotNumber', () => {
            let obj = mathEnforcer.sum('5', 5);
            assert.equal(obj, undefined);
        });

        it('shouldReturnUndefinedIfSecondNotNumber', () => {
            let obj = mathEnforcer.sum(5, '5');
            assert.equal(obj, undefined);
        });

        it('shouldReturnValidSum', () => {
            let obj = mathEnforcer.sum(15, 5);
            assert.equal(obj, 20);
        });

        it('shouldReturnValidNumberWhenFloating', () => {
            let obj = mathEnforcer.sum(5.5, 10);
            assert.equal(obj, 15.5);
        });

        it('shouldReturnValidNumber', () => {
            let obj = mathEnforcer.sum(-5, -10);
            assert.equal(obj, -15);
        });

        it('invalidWhenBothString', () => {
            let obj = mathEnforcer.sum('5', '-10');
            assert.equal(obj, undefined);
        });

        it("should return correct result for floating point parameters", function () {
            expect(mathEnforcer.sum(2.7, 3.4)).to.be.closeTo(6.1, 0.01);
        })
    });
});
    