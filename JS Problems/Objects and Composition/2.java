function worker(obj) {
    if (obj.dizziness) {
        obj.levelOfHydrated += Number(obj.weight * obj.experience / 10);
        obj.dizziness = false; 
    }

    return obj;
}
