function parseTownsToJSON(towns) {
    let townsArr = [];
    for (let town of towns.slice(1)) {
        let [empty, townName, lat, lng] =
            town.split(/\s*\|\s*/);
        let townObj = { Town: townName, Latitude:
                Math.round(Number(lat) * 100) / 100, Longitude: Math.round(Number(lng) * 100) / 100 };
        townsArr.push(townObj);
    }
    console.log(JSON.stringify(townsArr));;
}