function carFactory(car) {
    let demoCar = {
        model: 'Name',
        engine: {
            power: 0,
            volume: 0,
        },
        carriage: {
            type: 'name',
            color: 'color',
        },
        wheels: [],
    };

    let getModel = car.model;
    let getPower = car.power;
    let choosenColor = car.color;
    let choosenCarr = car.carriage;
    let wheelies = car.wheelsize;

    if (wheelies % 2 == 0) {
        wheelies -= 1;
    }

    demoCar.wheels.push(wheelies);
    demoCar.wheels.push(wheelies);
    demoCar.wheels.push(wheelies);
    demoCar.wheels.push(wheelies);


    demoCar.model = getModel;

    if (getPower <= 90) {
        demoCar.engine.power = 90;
        demoCar.engine.volume = 1800;
    } else if (getPower <= 120) {
        demoCar.engine.power = 120;
        demoCar.engine.volume = 2400;
    } else {
        demoCar.engine.power = 200;
        demoCar.engine.volume = 3500;
    }

    demoCar.carriage.type = choosenCarr;
    demoCar.carriage.color = choosenColor;

    return demoCar;
};