function heroicInventory(arr) {
    let result = [];

    for (let i = 0; i < arr.length; i++) {
        let splitWoItems = arr[i].split(' / ');
        items = splitWoItems[2] ? splitWoItems[2].split(', ') : [];
        
        result.push({
            name: splitWoItems[0],
            level: Number(splitWoItems[1]),
            items: items,
        });
    }

    console.log(JSON.stringify(result));
}

