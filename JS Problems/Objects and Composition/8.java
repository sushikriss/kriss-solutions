function rectangle(width, height, color) {
    color = color.charAt(0).toUpperCase() + color.slice(1);

    let obj = {
        width: Number(width),
        height: Number(height),
        color: color,
        calcArea: function() {
            return this.width * this.height;
        },
    };

    return obj;
}