function rotation(arr, rots) {
    let newArr = arr;

    for (let i = 0; i < rots; i++) {
        let lastElem = newArr.pop();
        newArr.unshift(lastElem);
    }

    console.log(newArr.join(' '));
}