function numbers(nums) {
    nums.sort((a, b) => a - b);

    let resultArr = [];

    while (nums.length) {
        resultArr.push(nums.shift());
        resultArr.push(nums.pop());
    }

    return resultArr.filter(num => {
        return num !== undefined;
    });
};