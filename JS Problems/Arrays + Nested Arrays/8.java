function sortingArr(words) {
    let sorted = words.sort((a, b) => {
        return a.length - b.length || a.localeCompare(b)
    });

    for (const s of sorted) {
        console.log(s);
    }
}