function toggle() {
    let buttonElement = document.getElementsByClassName("button")[0];
    let hiddenText = document.getElementById("extra");

    if (hiddenText.style.display === 'none') {
        hiddenText.style.display = 'block';
        buttonElement.textContent = "Less";
    } else {
        hiddenText.style.display = 'none';
        buttonElement.textContent = "More";
    }
    
}