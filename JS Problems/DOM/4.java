function search() {

   let listOfTowns = Array.from(document.getElementById('towns').getElementsByTagName('li'));
   let searchValue = document.getElementById('searchText').value; // text
   let foundMatches = document.getElementById('result');
   let found = 0;

   listOfTowns.forEach(x => {
      let element = x.textContent;
      if (element.includes(searchValue)) {
         found++;
         x.style.textDecoration = "underline";
         x.style.fontWeight = 'bold';
      }
   });

   foundMatches.textContent = `${found} matches found`;
}
