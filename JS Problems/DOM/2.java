function solve() {
  let textConvert = (text, value) => {
    text = text.toLowerCase();
    let textSplit = text.split(' ');
    let result = '';
    if (value === 'C') {
      result += textSplit[0];
      for (let i = 1; i < textSplit.length; i++) {
        let char = textSplit[i].charAt(0);
        result += textSplit[i].replace(char, char.toUpperCase());
      }
    } else {
      textSplit.reduce(function (acc, curr) {
        let char = curr.charAt(0);
        return result = acc + curr.replace(char, char.toUpperCase());
      }, result);
    }
    return result;
  }

  let textField = document.querySelector('#text').value;
  let conventionField = document.querySelector('#naming-convention').value;
  let res = document.getElementById("result");
  
  if (conventionField === 'Camel Case') {
    res.textContent += textConvert(textField, 'C');
  } else if (conventionField === 'Pascal Case') {
    res.textContent += textConvert(textField, 'P');
  } else {
    res.textContent += 'Error!';
  }

}