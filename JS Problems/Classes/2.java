class Request {
    response = undefined;
    fulfilled = false;

    constructor (method, uri, version, message) {
        this.method = method;
        this.message = message;
        this.version = version;
        this.uri = uri;
    }
}
