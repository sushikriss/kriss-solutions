function tickets(tickets, sortWord) {
    
    class Ticket {
        constructor (destination, price, status) {
            this.destination = destination;
            this.price = price;
            this.status = status;
        }
    }
        let allTicketsMap = [];
    
        tickets.forEach(el => {
            let save = el.split("|");
            
            let currentTicket = new Ticket(save[0], Number(save[1]), save[2]);
            allTicketsMap.push(currentTicket);
        });
    
        switch(sortWord) {
            case "destination":
                allTicketsMap = allTicketsMap.sort((a, b) => a.destination.localeCompare(b.destination));
                break;
            case "price":
                allTicketsMap = allTicketsMap.sort((a, b) => a.price - b.price);
                break;
            case "status":
                allTicketsMap = allTicketsMap.sort((a, b) => a.status.localeCompare(b.status));
                break;
        }
    
        return allTicketsMap;
    
    }
