function calculator() {
    let firstNumber;
    let secondNumber;
    let resultField;

    return {
        init: (selector1, selector2, result) => {
            firstNumber = document.querySelector(selector1);
            secondNumber = document.querySelector(selector2);
            resultField = document.querySelector(result);
        },
        add: () => {
            resultField.value = Number(firstNumber.value) + Number(secondNumber.value);
        },
        subtract: () => {
            resultField.value = Number(firstNumber.value) - Number(secondNumber.value);
        }
    }
}