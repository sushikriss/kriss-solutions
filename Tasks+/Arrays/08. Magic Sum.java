package com.company;
import java.util.Scanner;
public class Comissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] array = scanner.nextLine().split(" ");

        int[] arr = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            arr[i] = Integer.parseInt(array[i]);
        }

        int givenNumber = Integer.parseInt(scanner.nextLine());

        for (int i = 0; i < arr.length; i++) {
            int start = arr[i];

            for (int b = i + 1; b < arr.length; b++) {

                if (start + arr[b] == givenNumber) {
                    System.out.print(start + " " + arr[b]);
                    System.out.println();
                    break;
                }
            }
        }

    }
}
