package com.company;
import java.util.Scanner;
public class Comissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] array = scanner.nextLine().split(" ");

        int[] arr = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            arr[i] = Integer.parseInt(array[i]);
        }

        int rotations = Integer.parseInt(scanner.nextLine());

        // FINE

        for (int b = 0; b < rotations; b++) {

            int firstElement = arr[0];

            for (int c = 0; c < arr.length-1; c++) {
                arr[c] = arr[c+1];
            }

            arr[arr.length-1] = firstElement;
        }

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

    }
}

