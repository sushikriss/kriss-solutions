package map_ave;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] split = scanner.nextLine().split("\\\\");

        String lastOne = split[split.length - 1];

        String[] result = lastOne.split("\\.");

        System.out.println("File name: " + result[0]);
        System.out.println("File extension: " + result[1]);
    }
}