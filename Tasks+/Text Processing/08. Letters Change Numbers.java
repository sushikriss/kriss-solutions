package map_ave;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] words = scanner.nextLine().split("\\s+"); // A12b s17G
        double result = 0;
        for (String firstWord : words) {
            StringBuilder letters = new StringBuilder(); // Ab
            StringBuilder numbers = new StringBuilder(); // 12

            for (int i = 0; i < firstWord.length(); i++) {
                if (Character.isLetter(firstWord.charAt(i))) {
                    letters.append(firstWord.charAt(i));
                } else {
                    numbers.append(firstWord.charAt(i));
                }
            }

            double summer = Double.parseDouble(String.valueOf(numbers)); // 12
            char thing = letters.toString().toLowerCase().charAt(0);

            int position = 0;
            for (char i = 'a'; i <= 'z'; i++) {
                position++;
                if (thing == i) {
                    break;
                }
            }

            if (Character.isUpperCase(letters.charAt(0))) {
                summer /= position;
            } else {
                summer *= position;
            }

            position = 0;
            char thingT = letters.toString().toLowerCase().charAt(1);
            for (char i = 'a'; i <= 'z'; i++) {
                position++;
                if (thingT == i) {
                    break;
                }
            }

            if (Character.isUpperCase(letters.charAt(1))) {
                summer -= position;
            } else {
                summer += position;
            }

            result += summer;
        }

        System.out.printf("%.2f", result);

    }
}
