package map_ave;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] users = scanner.nextLine().split(", ");

        List<String> words = new ArrayList<>();

        for (String user : users) {
            boolean Add = true;

            if (user.length() > 3 && user.length() < 16) {
                for (int i = 0; i < user.length(); i++) {
                    char token = user.charAt(i);

                    if (!Character.isDigit(token) && !Character.isLetter(token)
                    && token != '_' && token != '-') {
                        Add = false;
                        break;
                    }
                }

                if (Add) {
                    words.add(user);
                }
            }
        }

        for (String word : words) {
            System.out.println(word);
        }


    }
}