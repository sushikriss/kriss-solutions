package map_ave;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] words = scanner.nextLine().split(" ");

        String oneE = words[0];
        String twoE = words[1];

        int length = Math.min(twoE.length(), oneE.length());

        int sum = 0;
        int counter = 0;

        for (int i = 0; i < length; i++) {
            char one = oneE.charAt(i);

            char two = twoE.charAt(i);

            sum += (int) one * (int) two;
            counter = length;
        }

        boolean ready = false;
        String word = "";

         if (twoE.length() > oneE.length()) {
            word = twoE;
            ready = true;
        } else if (oneE.length() > twoE.length()) {
            word = oneE;
            ready = true;
        }

        if (ready) {

            for (int i = counter; i < word.length(); i++) {
                sum += word.charAt(i);
            }
        }

        System.out.println(sum);


    }
}