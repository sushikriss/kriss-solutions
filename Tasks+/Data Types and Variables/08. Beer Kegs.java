package com.company;
import java.util.Scanner;
public class Comissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        double max = Double.MIN_VALUE;
        String best = "";

        for (int i = 0; i < n; i++) {
            String keg = scanner.nextLine();
            double radius = Double.parseDouble(scanner.nextLine());
            int height = Integer.parseInt(scanner.nextLine());
            double mod = Math.PI * Math.pow(radius,2) * height;

            if (mod > max) {
                best = keg;
                max = mod;
            }
        }

        System.out.println(best);



    }
}

