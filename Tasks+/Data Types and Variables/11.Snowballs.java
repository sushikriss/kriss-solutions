package com.company;
import java.util.Scanner;
public class Comissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int total = Integer.parseInt(scanner.nextLine());
        double max = Double.MIN_VALUE;
        double ss = 0, st = 0, sq = 0;

        for (int i = 0; i < total; i++) {
            double snowballSnow = Double.parseDouble(scanner.nextLine());
            double snowballTime = Double.parseDouble(scanner.nextLine());
            double snowballQuality = Double.parseDouble(scanner.nextLine());
            double formula = Math.pow((snowballSnow / snowballTime),snowballQuality);

            if (max < formula) {
                max = formula;
                ss = snowballSnow;
                st = snowballTime;
                sq = snowballQuality;
            }

        }

        System.out.printf("%.0f : %.0f = %.0f (%.0f)",ss,st,max,sq);

    }
}

