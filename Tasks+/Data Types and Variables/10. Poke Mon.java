package com.company;
import java.util.Scanner;
public class Comissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        double fifthyPercent = n * 0.5;

        int m = Integer.parseInt(scanner.nextLine());
        int y = Integer.parseInt(scanner.nextLine());
        int pokes = 0;

        while (n > m) {
            n -= m;
            pokes++;

            if (n == fifthyPercent ) {
                n = n / y;
            }
        }

        System.out.print(String.format("%d%n%d",n,pokes));

    }
}

