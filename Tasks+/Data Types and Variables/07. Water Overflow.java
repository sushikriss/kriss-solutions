package com.company;
import java.util.Scanner;
public class Comissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine()); // 255 capacity
        int totalIn = 0, capacity = 255;

        for (int i = 0; i < n; i++) {

            int liters = Integer.parseInt(scanner.nextLine());

            if (capacity - liters >= 0) {
                capacity -= liters;
                totalIn += liters;
            } else {
                System.out.println("Insufficient capacity!");
            }
        }
        System.out.println(totalIn);


    }
}

