package com.company;
import java.util.Scanner;
public class Comissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String command = scanner.nextLine();
        double inserted = 0;

        while (!command.equals("Start")) {

            double insert = Double.parseDouble(command);

            if (insert == 2 || insert == 1 || insert == 0.2 || insert == 0.1 || insert == 0.5) {
                inserted += insert;
            } else {
                System.out.printf("Cannot accept %.2f%n",insert);
            }

            command = scanner.nextLine();
        }

        double left = inserted;
        String newCommand = scanner.nextLine();
        while (!newCommand.equals("End")) {

            String product = newCommand;

            if (product.equals("Nuts") || product.equals("Water") || product.equals("Crisps") || product.equals("Soda") || product.equals("Coke")) {
                double price = 0;

                switch (product) {
                    case "Coke":
                        price = 1;
                        break;
                    case "Nuts":
                        price = 2.0;
                        break;
                    case "Water":
                        price = 0.7;
                        break;
                    case "Crisps":
                        price = 1.5;
                        break;
                    case "Soda":
                        price = 0.8;
                        break;

                }

                if (left - price >= 0) {
                    System.out.println("Purchased " + product);
                    left = left - price;
                }
                else {
                    System.out.println("Sorry, not enough money");
                    break;
                }

            } else {
                System.out.println("Invalid product");
            }
            newCommand = scanner.nextLine();
        }

        System.out.printf("Change: %.2f",left);




    }
}

