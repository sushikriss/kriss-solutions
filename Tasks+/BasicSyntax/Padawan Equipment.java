package com.company;
import java.util.Scanner;
public class Comissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double budget = Double.parseDouble(scanner.nextLine());
        int jedi = Integer.parseInt(scanner.nextLine());
        double lightSaberPrice = Double.parseDouble(scanner.nextLine());
        double robePrice = Double.parseDouble(scanner.nextLine());
        double belt = Double.parseDouble(scanner.nextLine());

        double totalLightSabers =  (Math.ceil(1.10 * jedi) * lightSaberPrice);
        double robes = robePrice * jedi;
        double belts = 0;

        for (int i = 1; i <= jedi; i++) {

            if (i % 6 != 0) {
                belts += belt;
            }
        }

        double sum = belts + robes + totalLightSabers;

        if (sum > budget) {
            System.out.printf("Ivan Cho will need %.2flv more.",sum-budget);
        } else {
            System.out.printf("The money is enough - it would cost %.2flv.",sum);
        }

    }
}

