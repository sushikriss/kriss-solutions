package com.company;
        import java.util.Scanner;
public class Comissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int lostGames = Integer.parseInt(scanner.nextLine());
        double headset = Double.parseDouble(scanner.nextLine());
        double mouse = Double.parseDouble(scanner.nextLine());
        double keyboard = Double.parseDouble(scanner.nextLine());
        double display = Double.parseDouble(scanner.nextLine());
        double expenses = 0;
        double kk = 0;

        for (int i = 1; i <= lostGames; i++) {
            int pupi = 0;

            if (i % 2 == 0) {
                expenses += headset;
                pupi++;
            }
            if (i % 3 == 0) {
                expenses += mouse;
                pupi++;
            }
            if (pupi >= 2) {
                expenses += keyboard;
                kk++;
            }
            if (kk % 2 == 0 && kk != 0) {
                expenses += display;
                kk = 0;
            }
        }

        System.out.printf("Rage expenses: %.2f lv.",expenses);
    }
}

