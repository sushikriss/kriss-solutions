package map_ave;
import java.util.LinkedHashMap;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        LinkedHashMap<String, String> user = new LinkedHashMap<>();

        for (int i = 0; i < n; i++) {
            String[] command = scanner.nextLine().split("\\s+");

            String name = command[1];
            if (command[0].equals("register")) {
                String licensePlate = command[2];

                if (!user.containsKey(name)) {
                    user.put(name, licensePlate);
                    System.out.printf("%s registered %s successfully%n", name, licensePlate);
                } else {
                    System.out.print(String.format("ERROR: already registered with plate number %s%n", user.get(name)));
                }

            }
            else {
                if (!user.containsKey(name)) {
                    System.out.printf("ERROR: user %s not found%n", name);
                } else {
                    System.out.printf("%s unregistered successfully%n", name);
                    user.remove(name);
                }

            }
        }

        user
                .entrySet()
                .stream()
                .forEach(k -> System.out.printf("%s => %s%n", k.getKey(), k.getValue()));

    }
}