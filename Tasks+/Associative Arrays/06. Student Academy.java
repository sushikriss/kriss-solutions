package map_ave;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int pairs = Integer.parseInt(scanner.nextLine());
        Map<String, ArrayList<Double>> student = new TreeMap<>();

        for (int i = 0; i < pairs; i++) {
            String name = scanner.nextLine();
            double grade = Double.parseDouble(scanner.nextLine());

            student.putIfAbsent(name, new ArrayList<>());
            student.get(name).add(grade);
        }

        student
                .entrySet()
                .stream()
                .filter(e ->
                        e.getValue()
                                .stream()
                                .mapToDouble(a -> Double.valueOf(a))
                                .average()
                                .getAsDouble() >= 4.50)
                .sorted((a1, a2) -> {
                    double first = a1.getValue().stream().mapToDouble(Double::doubleValue).average().getAsDouble();
                    double second = a2.getValue().stream().mapToDouble(Double::doubleValue).average().getAsDouble();
                    return Double.compare(second, first);
                })
                .forEach(s -> System.out.printf("%s -> %.2f%n", s.getKey(),
                        s.getValue().stream().mapToDouble(Double::doubleValue).average().getAsDouble()));
    }
}