package map_ave;

import java.util.Scanner;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        TreeMap<String, Integer> items = new TreeMap<>(); {{
            items.put("shards", 0);
            items.put("motes", 0);
            items.put("fragments", 0);
        }}

        TreeMap<String, Integer> junk = new TreeMap<>();

        boolean isObtained = false;

        while (!isObtained) {
            String[] tokens = scanner.nextLine().split(" ");
            for (int i = 0; i < tokens.length; i += 2) {
                int value = Integer.parseInt(tokens[i]);
                String type = tokens[i + 1].toLowerCase();

                if (items.containsKey(type)) {
                    addItems(items, type, value);
                    isObtained = hasLegendary(items, type);
                    if (isObtained) {
                        break;
                    }
                } else {
                    addItems(junk, type, value);
                }
            }

        }
        items
                .entrySet()
                .stream()
                .sorted((a1, a2) -> Integer.compare(a2.getValue(), a1.getValue()))
                .forEach(i -> System.out.printf("%s: %d%n", i.getKey(), i.getValue()));

        junk
                .entrySet()
                .forEach(i -> System.out.printf("%s: %d%n", i.getKey(), i.getValue()));


    }

    private static boolean hasLegendary(TreeMap<String, Integer> items, String type) {
        int count = items.get(type);
        if (count >= 250) {
            items.put(type, count - 250);
            switch (type) {
                case "shards":
                    System.out.println("Shadowmourne obtained!");
                    return true;
                case "motes":
                    System.out.println("Dragonwrath obtained!");
                    return true;
                case "fragments":
                    System.out.println("Valanyr obtained!");
                    return true;
            }
        }
        return false;
    }

    private static void addItems(TreeMap<String, Integer> item, String type, int value) {
        item.putIfAbsent(type, 0);
        int counter = item.get(type);
        item.put(type, counter + value);
    }
}
