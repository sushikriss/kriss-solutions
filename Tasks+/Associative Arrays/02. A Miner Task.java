package map_ave;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Map<String, Integer> resources = new LinkedHashMap<>();

        String command = scanner.nextLine();

        while (!command.equals("stop")) {
            int amount = Integer.parseInt(scanner.nextLine());

                resources.putIfAbsent(command, 0);
                int old = resources.get(command);
                resources.put(command, old + amount);


            command = scanner.nextLine();
        }

        resources.forEach((key, value) -> System.out.println(String.format("%s -> %d", key, value)));
    }
}
