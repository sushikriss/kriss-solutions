package map_ave;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char[] input = scanner.nextLine().toCharArray();

        Map<Character, Integer> letters = new LinkedHashMap<>();

        for (char c : input) {

            if (c == ' ') {
                continue;
            }

            letters.putIfAbsent(c, 0);
            int count = letters.get(c);
            letters.put(c, count + 1);
        }

        for (Map.Entry<Character, Integer> characterIntegerEntry : letters.entrySet()) {
            System.out.print(characterIntegerEntry.getKey() + " -> " + characterIntegerEntry.getValue());
            System.out.println();
        }
    }
}
