package map_ave;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        TreeMap<String, ArrayList<String>> companyId = new TreeMap<>();

        String input = scanner.nextLine();

        while (!input.equals("End")) {
            String[] ids = input.split(" -> ");
            String companyName = ids[0];
            String id = ids[1];

            if (companyId.containsKey(companyName)) {
                if (!companyId.get(companyName).contains(id)) {
                    companyId.get(companyName).add(id);
                }
            } else {
                companyId.put(companyName, new ArrayList<>());
                companyId.get(companyName).add(id);
            }


            input = scanner.nextLine();
        }

        companyId
                .entrySet()
                .stream()
                .forEach(entry -> {
                    System.out.printf("%s%n", entry.getKey());

                    for (int i = 0; i < entry.getValue().size(); i++) {
                        System.out.println("-- " + entry.getValue().get(i));
                    }
                });

    }
}