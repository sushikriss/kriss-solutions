package map_ave;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class shuusj {

    public static class Students {
        String firstName;
        String secondName;
        Double grade;

        private Students(String firstName, String secondName, Double grade) {
            this.firstName = firstName;
            this.secondName = secondName;
            this.grade = grade;
        }

        public double getGrade() {
            return this.grade;
        }

        @Override
        public String toString() {
            return String.format("%s %s: %.2f", this.firstName, this.secondName, this.grade);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Students> students = new ArrayList<>();
        int n = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < n; i++) {
            String[] info = scanner.nextLine().split(" ");
            String firstName = info[0];
            String secondName = info[1];
            double grade = Double.parseDouble(info[2]);
            Students student = new Students(firstName, secondName, grade);
            students.add(student);

        }
        students.sort(Comparator.comparingDouble(Students::getGrade).reversed());
        for (Students student : students) {
            System.out.println(student.toString());
        }
    }
}