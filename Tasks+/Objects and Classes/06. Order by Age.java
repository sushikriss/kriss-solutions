package map_ave;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class shuusj {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        String input = scanner.nextLine();

        List<Person> people = new ArrayList<>();
        while (!input.equals("End")) {

            String[] inputs = input.split(" ");
            String name = inputs[0];
            String id = inputs[1];
            int age = Integer.parseInt(inputs[2]);

            Person p = new Person(name, id, age);
            people.add(p);

            input = scanner.nextLine();
        }

        people.sort((f, s) -> f.getAge() - s.getAge());

        for (Person person : people) {
            System.out.println(String.format
                    ("%s with ID: %s is %d years old.",
                            person.getName(), person.getId(),
                            person.getAge()));
        }


}

static class Person {
        String name;
        String id;
        int age;

        Person(String name, String id, int age) {
            this.name = name;
            this.id = id;
            this.age = age;
        }

        public String getName() {
            return this.name;
        }

        public String getId() {
            return this.id;
        }

        public int getAge() {
            return this.age;
        }

    @Override
    public String toString() {
        return String.format("%s with ID: %s is %d years old.", this.name, this.id, this.age);
    }

    }
}