package map_ave;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String regex = ">>(\\w+)<<(\\d+\\.?\\d*)!(\\d+)";

        Pattern patter = Pattern.compile(regex);

        String text = scanner.nextLine();
        double total = 0;
        List<String> furniture = new ArrayList<>();

        while (!text.equals("Purchase")) {
            Matcher matcher = patter.matcher(text);

            while (matcher.find()) {
                furniture.add(matcher.group(1));
                double price = Double.parseDouble(matcher.group(2));
                double quantity = Double.parseDouble(matcher.group(3));
                total += price * quantity;
            }

            text = scanner.nextLine();
        }

        System.out.println("Bought furniture:");
        for (String s : furniture) {
            System.out.println(s);
        }
        System.out.printf("Total money spend: %.2f", total);

    }
}
