package map_ave;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<String> allSpeeders = Arrays.stream(scanner.nextLine().split(",\\s+")).collect(Collectors.toList());

        LinkedHashMap<String, Integer> cars = new LinkedHashMap<>();
        for (String racer : allSpeeders) {
            cars.putIfAbsent(racer, 0);
        }
        String input = scanner.nextLine();
        while (!input.equals("end of race")) {

            Pattern pattern = Pattern.compile("[A-Za-z]");
            Matcher matcher = pattern.matcher(input);
            StringBuilder racerName = new StringBuilder();
            while (matcher.find()) {
                racerName.append(matcher.group(0));
            }

            pattern = Pattern.compile("[\\d]");
            matcher = pattern.matcher(input);
            int racerDistance = 0;
            while (matcher.find()) {
                racerDistance += Integer.parseInt(matcher.group(0));
            }

            if (cars.containsKey(racerName.toString())) {
                cars.put(racerName.toString(), cars.get(racerName.toString()) + racerDistance);
            }

            input = scanner.nextLine();
        }

        List<String> winners = cars.entrySet().stream().
                sorted((f, s) -> s.getValue() - f.getValue()).
                limit(3).
                map(Map.Entry::getKey).
                collect(Collectors.toList());
        System.out.println(String.format("1st place: %s%n2nd place: %s%n3rd place: %s%n",
                winners.get(0),
                winners.get(1),
                winners.get(2)));

    }
}
