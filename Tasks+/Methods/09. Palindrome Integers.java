package com.company;
import java.util.Scanner;
public class Comissions {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String text = scanner.nextLine();

        while (!text.equals("END")) {
            
            int num = Integer.parseInt(text)
                    , reversedInteger = 0, remainder, originalInteger;
            originalInteger = num;

            while (num != 0) {
                remainder = num % 10;
                reversedInteger = reversedInteger * 10 + remainder;
                num /= 10;
            }

            if (originalInteger == reversedInteger)
                System.out.println("true");
            else
                System.out.println("false");

            text = scanner.nextLine();
        }

    }
}
