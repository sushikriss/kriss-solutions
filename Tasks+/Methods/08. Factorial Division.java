package com.company;
import java.util.Scanner;
public class Comissions {


    private static double factorialOfNumberOne(int num) {
            double fact = 1;
        for (int i = 1; i <= num; i++) {
            fact = fact*i;
        }
        return fact;

    }

    private static double factorialOfNumberTwo(int num) {
        double fact = 1;
        for (int i = 1; i <= num; i++) {
            fact = fact*i;
        }
        return fact;

    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num = Integer.parseInt(scanner.nextLine());
        int secondNum = Integer.parseInt(scanner.nextLine());
        System.out.println(String.format("%.2f",factorialOfNumberOne(num) / factorialOfNumberTwo(secondNum)));
    }



}
