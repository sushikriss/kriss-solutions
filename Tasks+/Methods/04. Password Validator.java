package com.company;
import java.util.Scanner;
public class Comissions {

    private static void between(String password) {
        if (password.length() < 6 || password.length() > 10) {
            System.out.println("Password must be between 6 and 10 characters");
        }
    }

    private static void consistence(String password) {

        int count = 0;
        for (int i = 0; i < password.length(); i++) {

            char sym = password.charAt(i);
            if (Character.isDigit(sym)) {
                count++;
            }
        }

        if (count < 2) {
            System.out.println("Password must have at least 2 digits");
        }
    }

    private static void digitOr(String password) {

        int counter = 0;
        for (int i = 0; i < password.length(); i++) {

            char sym = password.charAt(i);
            if (!Character.isLetter(sym)) {
                if (!Character.isDigit(sym)) {
                    counter++;
                }
            }
        }

        if (counter != 0) {
            System.out.println("Password must consist only of letters and digits");
        }
    }



    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String password = scanner.nextLine();

        between(password);
        digitOr(password);
        consistence(password);

        int counter = 0;
        int count = 0;

        for (int i = 0; i < password.length(); i++) {

            char sym = password.charAt(i);
            if (!Character.isLetter(sym)) {
                if (!Character.isDigit(sym)) {
                    count++;
                }
            }
        }

        if (count == 0) {
            counter++;
        }

        for (int i = 0; i < password.length(); i++) {

            char sym = password.charAt(i);
            if (Character.isDigit(sym)) {
                count++;
            }
        }

        if (count >= 2) {
            counter++;
        }
        if (password.length() >= 6 && password.length() <= 10) {
            counter++;
        }

        if(counter >= 3) {
            System.out.println("Password is valid");
        }


    }




}
