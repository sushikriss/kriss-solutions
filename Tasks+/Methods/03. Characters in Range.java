package com.company;

import java.util.Scanner;

public class Comissions {

    private static void inRange(char first, char second) {
        char ff;
        char ss;

        if (first < second) {
            ff = first;
            ss = second;
        } else {
            ff = second;
            ss = first;
        }

        for (int i = ff + 1; i < ss; i++) {
            System.out.print((char) i + " ");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char first = scanner.nextLine().charAt(0);
        char second = scanner.nextLine().charAt(0);
        inRange(first, second);
    }

}
