package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Comissions {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] numbers = scanner.nextLine().split("\\s+");
        int[] newNumbers = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            newNumbers[i] = Integer.parseInt(numbers[i]);
        }

        List<Integer> finalNumbers = new ArrayList<>();

        for (int i = 0; i < newNumbers.length; i++) {
            finalNumbers.add(i, newNumbers[i]);
        }

        String command = scanner.nextLine();

        while (!command.equals("end")) {

            String[] input = command.split("\\s+");

            if (input[0].equals("Delete")) {
                while (finalNumbers.contains(Integer.parseInt(input[1]))) {
                    finalNumbers.remove(Integer.valueOf(input[1]));
                }


            } else {
                finalNumbers.add(Integer.parseInt(input[2]), Integer.parseInt(input[1]));
            }

            command = scanner.nextLine();

        }

        for (int number : finalNumbers) {
            System.out.print(number + " ");
        }


    }
}
