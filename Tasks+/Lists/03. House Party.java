package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Commission {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int commandsTotal = Integer.parseInt(scanner.nextLine());

        List<String> partyCount = new ArrayList<>();

        for (int i = 0; i < commandsTotal; i++) {
            String[] command = scanner.nextLine().split(" ");
            String name = command[0];

            if (command[2].equalsIgnoreCase("going!")) {

                if (partyCount.contains(name)) {
                    System.out.println(name + " is already in the list!");
                } else {
                    partyCount.add(name);
                }
            }
            else if (command[2].equalsIgnoreCase("not")) {

                if (!partyCount.contains(name)) {
                    System.out.println(name + " is not in the list!");
                } else {
                    partyCount.removeAll(Collections.singleton(name));
                }
            }

        }

        for (int i = 0; i < partyCount.size(); i++) {
            String name = partyCount.get(i);
            System.out.println(name);
        }


    }
}
