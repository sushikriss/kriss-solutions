package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Commission {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] array = scanner.nextLine().split(" ");
        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            numbers.add(Integer.parseInt(array[i]));
        }

        String operation = scanner.nextLine();

        while (!operation.equalsIgnoreCase("end")) {
            String[] function = operation.split(" ");

            if (function[0].equals("Add")) {
                int num = Integer.parseInt(function[1]);
                numbers.add(num);
            } else if (function[0].equals("Insert")) {
                int num = Integer.parseInt(function[1]);
                int index = Integer.parseInt(function[2]);

                if (index >= 0 && index <= numbers.size()) {
                    numbers.add(index, num);
                }
                else {
                    System.out.println("Invalid index");
                }
            } else if (function[0].equals("Remove")) {
                int num = Integer.parseInt(function[1]);
                if (num >= 0 && num <= numbers.size()) {
                    numbers.remove(num);
                } else {
                    System.out.println("Invalid index");
                }
            } else if (function[0].equals("Shift")); {
                if (function[1].equals("Right")) {
                    
                } else {

                }
            }

            operation = scanner.nextLine();
        }



    }
}
