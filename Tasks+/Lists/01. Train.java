package com.company;
        import java.util.ArrayList;
        import java.util.List;
        import java.util.Scanner;

public class Comissions {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] train = scanner.nextLine().split(" ");
        int[] trainWag = new int[train.length];

        for (int i = 0; i < train.length; i++) {
            trainWag[i] = Integer.parseInt(train[i]);
        }

        List<Integer> wagons = new ArrayList<>();

        for (int i = 0; i < trainWag.length; i++) {
            wagons.add(i, trainWag[i]);
        }

        int capacity = Integer.parseInt(scanner.nextLine());
        String input = scanner.nextLine();

        while (!input.equals("end")) {
            String[] command = input.split(" ");

            if (command[0].equals("Add")) {
                int newWagon = Integer.parseInt(command[1]);
                wagons.add(newWagon);
            } else {
                int passengers = Integer.parseInt(command[0]);

                for (int i = 0; i < wagons.size();) {

                    if (passengers <= capacity - wagons.get(i)) {
                        wagons.set(i, passengers + wagons.get(i));
                        break;
                    } else {
                        i++;
                    }
                }
            }

            input = scanner.nextLine();
        }

        for (Integer wagon : wagons) {
            System.out.print(wagon + " ");
        }




    }
}
