package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Commission {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] array = scanner.nextLine().split(" ");
        List<Integer> firstPlayerCards = new ArrayList<>();

        for (String s : array) {
            firstPlayerCards.add(Integer.parseInt(s));
        }

        String[] arraySecond = scanner.nextLine().split(" ");
        List<Integer> secondPlayerCards = new ArrayList<>();

        for (String s : arraySecond) {
            secondPlayerCards.add(Integer.parseInt(s));
        }

        int sumFirst = 0;
        int sumSecond = 0;




        while (firstPlayerCards.size() > 0 && secondPlayerCards.size() > 0) {

                int firstCard = firstPlayerCards.get(0);
                int secondCard = secondPlayerCards.get(0);

                if (firstCard > secondCard) {
                    firstPlayerCards.add(firstCard);
                    firstPlayerCards.add(secondCard);
                    secondPlayerCards.remove(secondPlayerCards.get(0));
                    firstPlayerCards.remove(firstPlayerCards.get(0));


                } else if (secondCard > firstCard) {
                    secondPlayerCards.add(secondCard);
                    secondPlayerCards.add(firstCard);
                    firstPlayerCards.remove(firstPlayerCards.get(0));
                    secondPlayerCards.remove(secondPlayerCards.get(0));
                } else {
                    secondPlayerCards.remove(secondPlayerCards.get(0));
                    firstPlayerCards.remove(firstPlayerCards.get(0));
                }

        }

        for (Integer firstPlayerCard : firstPlayerCards) {
            sumFirst += firstPlayerCard;
        }

        for (Integer secondPlayerCard : secondPlayerCards) {
            sumSecond += secondPlayerCard;
        }

        if (sumFirst > sumSecond) {
            System.out.printf("First player wins! Sum: %d", sumFirst);
        } else {
            System.out.printf("Second player wins! Sum: %d", sumSecond);
        }







    }
}
