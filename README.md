# About

This repository contains some of my solutions to various problems that I've encountered and solved.  
The purpose of this repository is to document part of my journey as I continuously improve and develop my skills.

## Sources

- SoftUni
- HackerRank