ALTER TABLE users 
DROP PRIMARY KEY,
ADD PRIMARY KEY (`id`),
ADD CONSTRAINT username_field UNIQUE (id, username);