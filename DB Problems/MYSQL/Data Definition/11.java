CREATE TABLE `directors` (
`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
`director_name` VARCHAR(45) NOT NULL,
`notes` TEXT);

INSERT INTO `directors`
VALUES
(1, 'Charles', 'adsdasdsd'),
(2, 'Charles', NULL),
(3, 'Charles', 'adsdasdsd'),
(4, 'Charles', 'adsdasdsd'),
(5, 'Charles', 'adsdasdsd');

CREATE TABLE `genres` (
`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
`genre_name` VARCHAR(45) NOT NULL,
`notes` TEXT);

INSERT INTO `genres`
VALUES
(1, 'Charles', 'adsdasdsd'),
(2, 'Charles', NULL),
(3, 'Charles', 'adsdasdsd'),
(4, 'Charles', 'adsdasdsd'),
(5, 'Charles', 'adsdasdsd');

CREATE TABLE `categories` (
`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
`category_name` VARCHAR(45) NOT NULL,
`notes` TEXT);

INSERT INTO `categories`
VALUES
(1, 'Charles', 'adsdasdsd'),
(2, 'Charles', NULL),
(3, 'Charles', 'adsdasdsd'),
(4, 'Charles', 'adsdasdsd'),
(5, 'Charles', 'adsdasdsd');

CREATE TABLE `movies` (
`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
`title` VARCHAR(45) NOT NULL,
`director_id` INT NOT NULL,
`copyright_year` INT,
`length` INT,
`genre_id` INT,
`category_id` INT,
`rating` INT,
`notes` TEXT);

INSERT INTO `movies`
VALUES
(1, 'Charles', 2, 2006, 160, 12, 5, 9, 'adsadasd'),
(2, 'Charles', 2, 2006, 160, 12, 5, 9, 'adsadasd'),
(3, 'Charles', 2, 2006, 160, 12, 5, 9, 'adsadasd'),
(4, 'Charles', 2, 2006, 160, 12, 5, 9, 'adsadasd'),
(5, 'Charles', 2, 2006, 160, 12, 5, 9, 'adsadasd');



