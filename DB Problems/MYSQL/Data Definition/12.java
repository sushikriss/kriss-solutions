CREATE TABLE `categories` (
`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
`category` VARCHAR(45) NOT NULL,
`daily_rate` DOUBLE,
`weekly_rate` DOUBLE,
`monthly_rate` DOUBLE,
`weekend_rate` DOUBLE
);

INSERT INTO `categories`
VALUES
(1, 'Charles', 12, 321, 513, 1000),
(2, 'Charles', 12, 321, 513, 1000),
(3, 'Charles', 12, 321, 513, 1000);

CREATE TABLE `cars` (
`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
`plate_number` VARCHAR(45) NOT NULL,
`make` DOUBLE,
`model` VARCHAR(45),
`car_year` INT,
`category_id` INT,
`doors` INT,
`picture` MEDIUMBLOB,
`car_condition` VARCHAR(10) NOT NULL,
`available` BOOL
);

INSERT INTO `cars`
VALUES
(1, 'Charles123', 12.21, 'a2', 2010, 1, 4, NULL, 'good', true),
(2, 'Charles123', 12.21, 'a2', 2010, 1, 4, NULL, 'good', true),
(3, 'Charles123', 12.21, 'a2', 2010, 1, 4, NULL, 'good', true);

CREATE TABLE `employees` (
`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
`first_name` VARCHAR(45) NOT NULL,
`last_name` VARCHAR(45) NOT NULL,
`title` VARCHAR(30),
`notes` TEXT
);

INSERT INTO `employees`
VALUES
(1, 'Charles', 'James', 'whatever', 'blabla'),
(2, 'Charles', 'James', 'whatever', 'blabla'),
(3, 'Charles', 'James', 'whatever', 'blabla');

CREATE TABLE `customers` (
`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
`driver_license_number` VARCHAR(45) NOT NULL,
`full_name` VARCHAR(45) NOT NULL,
`address` VARCHAR(30),
`city` VARCHAR(20),
`zip_code` INT,
`notes` TEXT
);

INSERT INTO `customers`
VALUES
(1, 'CAPB2', 'James', 'whatever', 'blabla', 2, 'zzz'),
(2, 'CAPB2', 'James', 'whatever', 'blabla', 2, 'zzz'),
(3, 'CAPB2', 'James', 'whatever', 'blabla', 2, 'zzz');

CREATE TABLE `rental_orders` (
`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
`employee_id` INT NOT NULL,
`customer_id` INT NOT NULL,
`car_id` INT,
`car_condition` VARCHAR(20) NOT NULL,
`tank_level` INT NOT NULL,
`kilometrage_start` INT NOT NULL,
`kilometrage_end` INT NOT NULL,
`total_kilometrage` INT NOT NULL,
`start_date` DATETIME NOT NULL,
`end_date` DATETIME NOT NULL,
`total_days` INT NOT NULL,
`rate_applied` DOUBLE,
`tax_rate` DOUBLE,
`order_status` VARCHAR(45),
`notes` TEXT
);

INSERT INTO `rental_orders`
VALUES
(1, 222, 333, 5115, 'fine', 2, 100, 200, 300, '9999-12-31 23:59:59', '9222-12-31 23:59:59', 200, 21.21, 213.124, 'taken','azz'),
(2, 222, 333, 5115, 'fine', 2, 100, 200, 300, '9999-12-31 23:59:59', '9222-12-31 23:59:59', 200, 21.21, 213.124, 'taken','azz'),
(3, 222, 333, 5115, 'fine', 2, 100, 200, 300, '9999-12-31 23:59:59', '9222-12-31 23:59:59', 200, 21.21, 213.124, 'taken','azz');





