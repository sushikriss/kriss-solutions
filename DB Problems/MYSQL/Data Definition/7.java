CREATE TABLE `users` (
`id` INT UNIQUE AUTO_INCREMENT PRIMARY KEY,
`username` VARCHAR(30) NOT NULL UNIQUE,
`password` VARCHAR(26) NOT NULL,
`profile_picture` MEDIUMBLOB,
`last_login_time` TIMESTAMP,
`is_deleted` BOOLEAN
);

INSERT INTO `users` (`id`,`username`, `password`, `profile_picture`, `last_login_time`, `is_deleted`)
VALUES 
(1, 'Aleksander', '123', NULL, '2008-01-01 00:00:01', true),
(2, 'asd', '123', NULL, '2008-01-01 00:00:01', true),
(3, 'dasd', '123', NULL, '2008-01-01 00:00:01', true),
(4, 'ddaa', '123', NULL, '2008-01-01 00:00:01', true),
(5, 'dzzd', '123', NULL, '2008-01-01 00:00:01', true)


