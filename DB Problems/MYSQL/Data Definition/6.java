CREATE TABLE `people` (
`id` INT UNIQUE AUTO_INCREMENT,
`name` VARCHAR(200) NOT NULL,
`picture` BLOB,
`height` DOUBLE,
`weight` DOUBLE,
`gender` CHAR(1) NOT NULL,
`birthdate` DATETIME NOT NULL,
`biography` TEXT,
PRIMARY KEY (`id`)
);

INSERT INTO `people` (`id`,`name`, `picture`, `height`, `weight`, `gender`, `birthdate`, `biography`)
VALUES (1, 'Aleksander', NULL, 0.85, 13, 'm', '2018-11-09', 'Sashko'),
(2, 'Aleksander', NULL, 0.85, 13, 'm', '2018-11-09', 'Sashko'),
(3, 'Aleksander', NULL, 0.85, 13, 'm', '2018-11-09', 'Sashko'),
(4, 'Aleksander', NULL, 0.85, 13, 'm', '2018-11-09', 'Sashko'),
(5, 'Aleksander', NULL, 0.85, 13, 'm', '2018-11-09', 'Sashko');


