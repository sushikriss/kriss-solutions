SELECT
CASE
WHEN `deposit_group` = 'Troll Chest' THEN LEFT(`first_name` , 1)
END
AS `first_letter` FROM `wizzard_deposits`
GROUP BY `first_letter`
ORDER BY `first_letter`