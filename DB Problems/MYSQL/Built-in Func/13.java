SELECT `user_name`,
SUBSTRING(`email`, LOCATE('@', `email`) + 1) AS 'provider'
FROM `users`
ORDER BY `provider`, `user_name`;