public class Truck extends Vehicle {
    public Truck(double fuelQuantity, double fuelConsumption) {
        super(fuelQuantity, fuelConsumption);
        setFuelConsumption(fuelConsumption);
    }

    @Override
    public double getFuelQuantity() {
        return super.getFuelQuantity();
    }

    @Override
    public void setFuelConsumption(double fuelConsumption) {
        super.setFuelConsumption(fuelConsumption + 1.6);
    }

    @Override
    public double getFuelConsumption() {
        return super.getFuelConsumption();
    }

    @Override
    public void refuel(double amount) {
        super.refuel(amount * 0.95);
    }

    public boolean drive(double kmsToGo) {
        double finalKilometers = kmsToGo * getFuelConsumption();
        if (getFuelQuantity() >= finalKilometers) {
            setFuelQuantity(getFuelQuantity() - finalKilometers);
            return true;
        } else {
            System.out.println("Truck needs refueling");
            return false;
        }
    }
}
