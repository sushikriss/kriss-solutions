public class Car extends Vehicle {
    public Car(double fuelQuantity, double fuelConsumption, double tankCapacity) {
        super(fuelQuantity, fuelConsumption, tankCapacity);
        setFuelQuantity(fuelQuantity);
    }

    @Override
    public double getFuelQuantity() {
        return super.getFuelQuantity();
    }

    @Override
    public void setFuelConsumption(double fuelConsumption) {
        super.setFuelConsumption(fuelConsumption + 0.9);
    }

    @Override
    public double getFuelConsumption() {
        return super.getFuelConsumption();
    }

    @Override
    public void refuel(double amount) {
        super.refuel(amount);
    }

    public boolean drive(double kmsToGo) {
        double finalKilometers = kmsToGo * getFuelConsumption();
        if (getFuelQuantity() >= finalKilometers) {
            setFuelQuantity(getFuelQuantity() - finalKilometers);
            return true;
        } else {
            System.out.println("Car needs refueling");
            return false;
        }
    }


}
