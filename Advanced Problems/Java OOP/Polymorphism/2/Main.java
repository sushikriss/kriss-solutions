import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] carInfo = scanner.nextLine().split("\\s+");
        String[] truckInfo = scanner.nextLine().split("\\s+");
        String[] busInfo = scanner.nextLine().split("\\s+");

        Car car = new Car(Double.parseDouble(carInfo[1]), Double.parseDouble(carInfo[2]), Double.parseDouble(carInfo[3]));
        Truck truck = new Truck(Double.parseDouble(truckInfo[1]), Double.parseDouble(truckInfo[2]), Double.parseDouble(truckInfo[3]));
        Bus bus = new Bus(Double.parseDouble(busInfo[1]), Double.parseDouble(busInfo[2]), Double.parseDouble(busInfo[3]));

        int n = Integer.parseInt(scanner.nextLine());

        while (n-- > 0) {
            String[] command = scanner.nextLine().split("\\s+");
            String vehicle = command[1];
            String doingWhat = command[0];
            double amount = Double.parseDouble(command[2]);

            NumberFormat format = new DecimalFormat("#.##");
            format.setMinimumFractionDigits(0);

            switch (vehicle) {
                case "Car":
                    if (doingWhat.equals("Drive")) {
                        if (car.drive(amount)) {
                            System.out.println("Car travelled " + format.format(amount) + " km");
                        }
                    } else {
                        car.refuel(amount);
                    }
                    break;
                case "Truck":
                    if (doingWhat.equals("Drive")) {
                        if (truck.drive(amount)) {
                            System.out.println("Truck travelled " + format.format(amount) + " km");
                        }
                    } else {
                        truck.refuel(amount);
                    }
                    break;
                case "Bus":
                    if (doingWhat.equals("Drive")) {
                        if (bus.drivePeople(amount)) {
                            System.out.println("Bus travelled " + format.format(amount) + " km");
                        }
                    } else if (doingWhat.equals("DriveEmpty")) {
                        if (bus.driveEmpty(amount)) {
                            System.out.println("Bus travelled " + format.format(amount) + " km");
                        }
                    } else {
                        bus.refuel(amount);
                    }
            }
        }

        System.out.printf("Car: %.2f%n", car.getFuelQuantity());
        System.out.printf("Truck: %.2f%n", truck.getFuelQuantity());
        System.out.printf("Bus: %.2f%n", bus.getFuelQuantity());

    }
}