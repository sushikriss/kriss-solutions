public class Bus extends Vehicle {
    public Bus(double fuelQuantity, double fuelConsumption, double tankCapacity) {
        super(fuelQuantity, fuelConsumption, tankCapacity);
        setFuelQuantity(fuelQuantity);
    }

    @Override
    public double getFuelQuantity() {
        return super.getFuelQuantity();
    }

    @Override
    public void setFuelConsumption(double fuelConsumption) {
        super.setFuelConsumption(fuelConsumption);
    }

    @Override
    public double getFuelConsumption() {
        return super.getFuelConsumption();
    }

    @Override
    public void refuel(double amount) {
        super.refuel(amount);
    }

    public boolean drivePeople(double distance) {
        double finalKilometers = (getFuelConsumption() + 1.4) * distance;
        if (getFuelQuantity() >= finalKilometers) {
            setFuelQuantity(getFuelQuantity() - finalKilometers);
            return true;
        } else {
            System.out.println("Bus needs refueling");
            return false;
        }
    }

    public boolean driveEmpty(double distance) {
        double finalKilometers = getFuelConsumption() * distance;
        if (getFuelQuantity() >= finalKilometers) {
            setFuelQuantity(getFuelQuantity() - finalKilometers);
            return true;
        } else {
            System.out.println("Bus needs refueling");
            return false;
        }
    }


}
