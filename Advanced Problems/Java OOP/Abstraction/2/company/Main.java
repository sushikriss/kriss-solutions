package com.company;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String card = scanner.nextLine();
        String suitPow = scanner.nextLine();

        CardRank cardRank = CardRank.valueOf(card.toLowerCase());
        CardSuit cardSuit = CardSuit.valueOf(suitPow);

        System.out.printf("Card name: %s of %s; Card power: %d",
                card, cardSuit, cardRank.getRank() + cardSuit.getSuitPow());
    }
}
