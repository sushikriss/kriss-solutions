package com.company;

public enum CardSuit {
    CLUBS(0),
    DIAMONDS(13),
    HEARTS(26),
    SPADES(39);

    private int suitPow;

    CardSuit(int suitPow) {
        this.suitPow = suitPow;
    }

    public int getSuitPow() {
        return this.suitPow;
    }
}
