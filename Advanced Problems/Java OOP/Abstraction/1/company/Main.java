package com.company;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Card Ranks:");
        for (CardRanks value : CardRanks.values()) {
            System.out.println("Ordinal value: " + value.ordinal() + "; "
                    + "Name value: " + value.name().toUpperCase());
        }

    }
}
