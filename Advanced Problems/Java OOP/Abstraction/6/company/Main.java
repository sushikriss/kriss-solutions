package com.company;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Card Suits:");
        for (CardSuits value : CardSuits.values()) {
            System.out.println("Ordinal value: " + value.ordinal() + "; "
                    + "Name value: " + value.name());
        }

    }
}
