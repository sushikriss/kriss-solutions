package blackBoxInteger;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        Scanner scanner = new Scanner(System.in);
        String line;

        Constructor<?>[] declaredConstructors = BlackBoxInt.class.getDeclaredConstructors();
        BlackBoxInt blackBoxInt = null;

        try {
            Constructor<?> declaredConstructor = BlackBoxInt.class.getDeclaredConstructor();
            declaredConstructor.setAccessible(true);
            blackBoxInt = (BlackBoxInt) declaredConstructor.newInstance();

        } catch (NoSuchMethodException | IllegalArgumentException | InstantiationException | InvocationTargetException | IllegalAccessException ex) {
            ex.printStackTrace();
        }

        Map<String, Method> methods = new HashMap<>();

        for (Method declaredMethod : blackBoxInt.getClass().getDeclaredMethods()) {
            methods.put(declaredMethod.getName(), declaredMethod);
        }

        while (!(line = scanner.nextLine()).equals("END")) {
            String[] tokens = line.split("_");

            Method method = methods.get(tokens[0]);
            method.setAccessible(true);
            method.invoke(blackBoxInt, Integer.parseInt(tokens[1]));
            Field declaredField = blackBoxInt.getClass().getDeclaredFields()[1];
            declaredField.setAccessible(true);
            System.out.println(declaredField.getInt(blackBoxInt));

        }

    }
}
