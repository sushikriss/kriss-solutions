package harvestingFields;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
public class Main {
	public static void main(String[] args) throws IOException, NoSuchFieldException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		Field[] field = RichSoilLand.class.getDeclaredFields();

		String command;
		while (!(command = reader.readLine()).equals("HARVEST")) {

			switch (command) {
				case "private":
					for (Field fieldType : field) {
						if (Modifier.isPrivate(fieldType.getModifiers())) {
							System.out.printf("private %s %s%n",
									fieldType.getType().getSimpleName(),
									fieldType.getName());
						}
					}
					break;
				case "public":
					for (Field fieldType : field) {
						if (Modifier.isPublic(fieldType.getModifiers())) {
							System.out.printf("public %s %s%n",
									fieldType.getType().getSimpleName(),
									fieldType.getName());
						}
					}
					break;
				case "protected":
					for (Field fieldType : field) {
						if (Modifier.isProtected(fieldType.getModifiers())) {
							System.out.printf("protected %s %s%n",
									fieldType.getType().getSimpleName(),
									fieldType.getName());
						}
					}
					break;
				case "all":
					for (Field fieldType : field) {
						String modifier = Modifier.toString(fieldType.getModifiers());
						System.out.printf("%s %s %s%n",
								modifier,
								fieldType.getType().getSimpleName(),
								fieldType.getName());
					}
			}
		}
	}
}
