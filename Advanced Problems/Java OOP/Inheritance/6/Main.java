import animals.*;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String type;

        while (!(type = scanner.nextLine()).equals("Beast!")) {
            String[] input = scanner.nextLine().split(" ");
            String name = input[0];
            int age = Integer.parseInt(input[1]);
            String gender = input[2];

            try {
                Animal animal = Animal.createAnimal(type, name, age, gender);
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        }

    }
}
