import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());

        List<Citizen> citizens = new ArrayList<>();
        List<Rebel> rebels = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            String[] tokens = scanner.nextLine().split(" ");

            if (tokens.length > 3) {
                citizens.add(new Citizen(tokens[0],
                        Integer.parseInt(tokens[1]),
                        tokens[2],
                        tokens[3]));
            } else {
                rebels.add(new Rebel(tokens[0],
                        Integer.parseInt(tokens[1]),
                        tokens[2]));
            }
        }
        int total = 0;
        String name;
        while (!(name = scanner.nextLine()).equals("End")) {
            for (Rebel rebel : rebels) {
                if (rebel.getName().equals(name)) {
                    total += 5;
                }
            }
            for (Citizen citizen : citizens) {
                if (citizen.getName().equals(name)) {
                    total += 10;
                }
            }
        }
        System.out.println(total);
    }
}

