import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Birthable> births = new ArrayList<>();

        String read;
        while (!(read = scanner.nextLine()).equals("End")) {
            String[] tokens = read.split(" ");

            switch (tokens[0]) {
                case "Citizen":
                    births.add(new Citizen(tokens[1],
                            Integer.parseInt(tokens[2]),
                            tokens[3],
                            tokens[4]));
                    break;
                case "Pet":
                    births.add(new Pet(tokens[1], tokens[2]));
                    break;
            }
        }

        String date = scanner.nextLine();

        for (Birthable birth : births) {
            if (birth.getBirthDate().endsWith(date)) {
                System.out.println(birth.getBirthDate());
            }
        }
    }
}

