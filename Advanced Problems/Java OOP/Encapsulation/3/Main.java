import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] persons = scanner.nextLine().split(";");
        LinkedHashSet<Person> personsSet = new LinkedHashSet<>();

        for (String value : persons) {
            String[] split = value.split("=");

            String name = split[0];
            double money = Double.parseDouble(split[1]);

            Person person = new Person(name, money);
            personsSet.add(person);
        }

        String[] products = scanner.nextLine().split(";");
        ArrayList<Product> productsSet = new ArrayList<>();

        for (String s : products) {
            String[] prices = s.split("=");

            String productName = prices[0];
            double productCost = Double.parseDouble(prices[1]);

            Product product = new Product(productName, productCost);
            productsSet.add(product);
        }

        String command = scanner.nextLine();

        while (!command.equals("END")) {
            String[] info = command.split("\\s+");

            String name = info[0];
            String product = info[1];

            for (Person person : personsSet) {
                if (person.getName().equals(name)) {
                    for (Product prod : productsSet) {
                        if (prod.getName().equals(product)) {
                            person.buyProduct(prod);
                        }
                    }
                }
            }


            command = scanner.nextLine();
        }

        for (Person person : personsSet) {
            if (person.getProductsSize() <= 0) {
                System.out.printf("%s - Nothing bought%n", person.getName().trim());
            } else {
                System.out.println(person.toString());
            }
        }

    }
}
