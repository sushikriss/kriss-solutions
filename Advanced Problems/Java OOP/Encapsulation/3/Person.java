import java.util.ArrayList;
import java.util.List;

public class Person {
    private String name;
    private double money;
    private List<Product> products;

    public Person(String name, double money) {
        setName(name);
        setMoney(money);
        this.products = new ArrayList<>();
    }

    private void setName(String name) {
        if (name == null || name.trim().isEmpty() || name.length() == 0) {
            throw new IllegalArgumentException("Name cannot be empty");
        }
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public double getMoney() {
        return this.money;
    }

    public int getProductsSize() {
        return this.products.size();
    }

    private void setMoney(double money) {
        if (money <= 0) {
            throw new IllegalArgumentException("Money cannot be negative");
        }
        this.money = money;
    }

    public void buyProduct(Product product) {
        if (getMoney() >= product.getCost()) {
            this.money -= product.getCost();
            this.products.add(product);
            System.out.printf("%s bought %s%n", getName().trim(), product.getName());
        } else {
            System.out.printf("%s can't afford %s%n", getName().trim(), product.getName());
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getName().trim()).append(" - ");
        for (Product product : products) {
            sb.append(product.getName());
            sb.append(", ");

        }
        sb.deleteCharAt(sb.length() - 1);
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}
