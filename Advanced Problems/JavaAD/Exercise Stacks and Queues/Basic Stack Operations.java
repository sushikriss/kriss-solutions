package com.company;
import java.util.*;

public class Kriss {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] comm = scanner.nextLine().split(" ");

        int[] commands = new int[comm.length];
        for (int i = 0; i < commands.length; i++) {
            commands[i] = Integer.parseInt(comm[i]);
        }

        int totalPushed = commands[0];
        int popped = commands[1];
        int presentOrNo = commands[2];

        String[] comma = scanner.nextLine().split(" ");

        int[] numbersRead = new int[comma.length];
        for (int i = 0; i < numbersRead.length; i++) {
            numbersRead[i] = Integer.parseInt(comma[i]);
        }

        ArrayDeque<Integer> numbers = new ArrayDeque<>();

        for (int i = 0; i < totalPushed; i++) {
            numbers.push(numbersRead[i]);
        }

        for (int i = 1; i <= popped; i++) {
            numbers.pop();
        }

        if (numbers.contains(presentOrNo)) {
            System.out.println("true");
        } else if (numbers.size() < 1) {
            System.out.println("0");
        } else {
            int min = Integer.MAX_VALUE; // 100
            int size = numbers.size();

            for (int i = 0; i < size; i++) {

                int num = numbers.pop();

                if (num < min) {
                    min = num;
                }
            }

            System.out.println(min);
        }


    }
}
