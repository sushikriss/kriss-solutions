package com.company;

import java.util.*;

public class Kriss {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] numbers = scanner.nextLine().split("\\s+");

        ArrayDeque<Integer> stack = new ArrayDeque<>();

        for (int i = 0; i < numbers.length; i++) {
            stack.push(Integer.parseInt(numbers[i]));  // 1 2 3 4 5
        }

        System.out.println(stack.toString().replaceAll("[\\[|\\]|,]", ""));

    }
}
