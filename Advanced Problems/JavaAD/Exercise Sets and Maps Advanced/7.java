package com.company; // USE COLLECTIONS MORE OFTEN :) // read carefully c:
import java.util.*;

public class Kriss {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Map<String, ArrayList<String>> nameAndCards = new LinkedHashMap<>();

        String input = scanner.nextLine();
        while (!input.equals("JOKER")) {

            String[] nameWithCards = input.split(": ");
            String name = nameWithCards[0]; // JORGE

            String cardsNotSeparated = nameWithCards[1];
            String[] cards = cardsNotSeparated.split(", "); // 2A B5 C7

            for (int i = 0; i < cards.length; i++) {

                if (!nameAndCards.containsKey(name)) {
                    nameAndCards.put(name, new ArrayList<>());
                    nameAndCards.get(name).add(cards[i]);
                } else {
                    if (!nameAndCards.get(name).contains(cards[i])) {
                        nameAndCards.get(name).add(cards[i]);
                    }
                }
            }

            input = scanner.nextLine();
        }

        Map<String, Integer> nameAndPoints = new LinkedHashMap<>();

        nameAndCards
                .forEach((key, value) -> {
                    String name = key;

                    for (int i = 0; i < value.size(); i++) {
                        String card = value.get(i);

                        int firstSum = 0;
                        int secondSum = 0;
                        int total = 0;

                        if (card.length() > 2) {
                            firstSum = 10;
                            char letter = card.charAt(2);

                            switch (letter) {
                                case 'S':
                                    secondSum = 4;
                                    break;
                                case 'H':
                                    secondSum = 3;
                                    break;
                                case 'D':
                                    secondSum = 2;
                                    break;
                                case 'C':
                                    secondSum = 1;
                                    break;
                            }
                        } else {

                            char first = card.charAt(0);
                            char second = card.charAt(1);

                            switch (first) {
                                case 'J':
                                    firstSum = 11;
                                    break;
                                case 'Q':
                                    firstSum = 12;
                                    break;
                                case 'K':
                                    firstSum = 13;
                                    break;
                                case 'A':
                                    firstSum = 14;
                                    break;
                                default: firstSum = Integer.parseInt(String.valueOf(first));
                            }

                            switch (second) {
                                case 'S':
                                    secondSum = 4;
                                    break;
                                case 'H':
                                    secondSum = 3;
                                    break;
                                case 'D':
                                    secondSum = 2;
                                    break;
                                case 'C':
                                    secondSum = 1;
                                    break;
                                default: secondSum = Integer.parseInt(String.valueOf(second));
                            }
                        }
                        total = firstSum * secondSum;


                        if (nameAndPoints.containsKey(name)) {
                            int valueEx = nameAndPoints.get(name);
                            nameAndPoints.put(name, valueEx + total);
                        } else {
                            nameAndPoints.put(name, total);
                        }
                    }
                });

                nameAndPoints
                        .forEach((key, value) -> System.out.printf("%s: %d%n", key, value));



    }
}