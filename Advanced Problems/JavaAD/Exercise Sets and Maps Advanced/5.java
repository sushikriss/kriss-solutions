package com.company; // USE COLLECTIONS MORE OFTEN :)

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
public class Kriss {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Map<String, String> library = new HashMap<>();

        String text = scanner.nextLine();

        while (!text.equals("search")) {

            String[] input = text.split("-");

            String name = input[0];
            String number = input[1];

            library.put(name, number);

            text = scanner.nextLine();
        }

        String searchingFor = scanner.nextLine();

        while (!searchingFor.equals("stop")) {

            if (library.containsKey(searchingFor)) {
                System.out.print(searchingFor + " -> ");
                System.out.println(library.get(searchingFor));
            } else {
                System.out.println("Contact " + searchingFor + " does not exist.");
            }

            searchingFor = scanner.nextLine();
        }




    }
}