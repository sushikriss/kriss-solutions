package com.company; // USE COLLECTIONS MORE OFTEN :) // read carefully c:
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
public class Kriss {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Map<String, String> nameAndMail = new LinkedHashMap<>();

        String name = scanner.nextLine();

        while (!name.equals("stop")) {

            String mail = scanner.nextLine();
            String[] mailDt = mail.split("\\.");

            if (!mailDt[1].equals("com") && !mailDt[1].equals("uk") && !mailDt[1].equals("us")) {
                nameAndMail.put(name, mail);
            }

            name = scanner.nextLine();
        }

        nameAndMail
                .forEach((key, value) ->
                        System.out.printf("%s -> %s%n", key, value));




    }
}