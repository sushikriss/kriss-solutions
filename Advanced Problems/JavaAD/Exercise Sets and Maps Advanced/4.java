package com.company; // USE COLLECTIONS MORE OFTEN :) // Read really carefully :c
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Kriss {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();

        TreeMap<Character, Integer> finish = new TreeMap<>();

        for (int i = 0; i < input.length(); i++) {

            char sym = (char) input.charAt(i);

            int counter = 0;
            for (int j = 0; j < input.length(); j++) {
                if (sym == input.charAt(j)) {
                    counter++;
                }
            }
            finish.put(sym, counter);
        }

        finish
                .forEach((key, value) -> System.out.println(key + ": "
                        + value + " time/s"));

    }
}