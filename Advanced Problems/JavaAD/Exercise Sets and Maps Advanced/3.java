package com.company; // USE COLLECTIONS MORE OFTEN :) // Read really carefully :c

import java.util.Arrays;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
public class Kriss {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());

        Set<String> chemicals = new TreeSet<>();

        for (int i = 0; i < n; i++) {
            String[] input = scanner.nextLine().split(" ");

            chemicals.addAll(Arrays.asList(input));
        }

        for (String chemical : chemicals) {
            System.out.print(chemical + " ");
        }

    }
}