package com.company; // USE COLLECTIONS MORE OFTEN :) // Read really carefully :c
import java.util.*;
public class Kriss {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] ns = scanner.nextLine().split(" ");

        int n = Integer.parseInt(ns[0]);
        int m = Integer.parseInt(ns[1]);

        Set<Integer> firstSet = new LinkedHashSet<>();

        Set<Integer> secondSet = new LinkedHashSet<>();

        for (int i = 0; i < n; i++) {
            int input = Integer.parseInt(scanner.nextLine());

            firstSet.add(input);
        }

        for (int i = 0; i < m; i++) {
            int input = Integer.parseInt(scanner.nextLine());

            secondSet.add(input);
        }

        firstSet.retainAll(secondSet);

        Set<Integer> finalized = new LinkedHashSet<>(firstSet);

        for (Integer integer : finalized) {
            System.out.print(integer + " ");
        }


    }
}