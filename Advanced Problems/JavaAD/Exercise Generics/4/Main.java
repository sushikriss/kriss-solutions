package com.company; // USE COLLECTIONS MORE OFTEN :) // read carefully c:

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));

        int count = Integer.parseInt(scanner.readLine());

        List<Box<String>> boxes = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            boxes.add(new Box<>(scanner.readLine()));
        }

        int[] index = Arrays.stream(scanner.readLine().split(" "))
                .mapToInt(Integer::parseInt).toArray();
        swaper(boxes, index[0], index[1]);

        for (Box<String> box : boxes) {
            System.out.println(box);
        }
    }

    private static <E> void swaper(List<E> list, int indexF, int indexS) {
        E firstElement = list.get(indexF);
        E secondElement = list.get(indexS);

        list.set(indexF, secondElement);
        list.set(indexS, firstElement);
    }
}
