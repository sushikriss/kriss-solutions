package com.company; // USE COLLECTIONS MORE OFTEN :) // read carefully c:

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Box<Double>> boxes = new ArrayList<Box<Double>>();

        int count = Integer.parseInt(reader.readLine());

        for (int i = 0; i < count; i++) {
            double value = Double.parseDouble(reader.readLine());

            boxes.add(new Box<Double>(value));
        }

        double element = Double.parseDouble(reader.readLine());

        System.out.println(CountGreater(boxes, element));
    }

    private static <T extends java.lang.Comparable<T>> int CountGreater(java.lang.Iterable<Box<T>> collection, T element) {
        int counter = 0;

        for (Box<T> item : collection) {
            if (item.compareTo(element) > 0) {
                counter++;
            }
        }

        return counter;
    }
}
