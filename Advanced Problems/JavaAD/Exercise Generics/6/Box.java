package com.company;

public class Box<T extends java.lang.Comparable<T>> implements java.lang.Comparable<T>
{
    private T value;

    public Box(T value)
    {
        this.value = value;
    }

    public final int compareTo(T other)
    {
        return this.value.compareTo(other);
    }

    @Override
    public String toString()
    {
        return String.format("%1$s: %2$s", this.value.getClass().getName(), this.value);
    }
}