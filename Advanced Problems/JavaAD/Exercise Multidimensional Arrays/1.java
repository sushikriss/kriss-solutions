package com.company; // USE COLLECTIONS MORE OFTEN :)

import java.util.*;

public class Kriss {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(", ");

        int n = Integer.parseInt(input[0]);
        String pattern = input[1];

        int[][] matrix = new int[n][n];

        if (pattern.equals("A")) {
            matrixColum(matrix, n);
        } else {
            matrixColumed(matrix, n);
        }

        for (int[] ints : matrix) {
            for (int anInt : ints) {
                System.out.print(anInt + " ");
            }
            System.out.println();
        }
    }

    private static int[][] matrixColumed(int[][] matrixAsa, int n) {
        int one = 1;
        int start = 0;

        for (int i = 0; i < n; i++) {
            if (one % 2 == 0) {

                for (int j = n-1; j >= 0; j--) {
                    matrixAsa[j][i] = ++start;
                }
            }
            else {
                for (int j = 0; j < n; j++) {
                    matrixAsa[j][i] = ++start;
                }
            }
            one++;
        }
        return matrixAsa;
    }

    private static int[][] matrixColum(int[][] matrixVol, int n) {
        int starter = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrixVol[j][i] = ++starter;
            }
        }
        return  matrixVol;
    }
}