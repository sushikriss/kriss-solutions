package com.company; // USE COLLECTIONS MORE OFTEN :)
import java.util.*;
public class Kriss {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(" ");
        int rows = Integer.parseInt(input[0]);
        int cols = Integer.parseInt(input[1]);

        String[][] matrix = new String[rows][cols];

        for (int i = 0; i < rows; i++) {
            String[] arr = scanner.nextLine().split(" ");

            matrix[i] = arr;
        }

        String scan = scanner.nextLine();

        while (!scan.equals("END")) {
            String[] data = scan.split(" ");

            if (data[0].equals("swap")) {
                int rowsFirst = Integer.parseInt(data[1]);
                int colsFirst = Integer.parseInt(data[2]);
                int rowsSecond = Integer.parseInt(data[3]);
                int colsSecond = Integer.parseInt(data[4]);

                if (rowsFirst < 0 || rowsFirst > rows || colsFirst < 0 || colsFirst > cols
                || rowsSecond > rows || rowsSecond < 0 || colsSecond > cols || colsSecond < 0) {
                    System.out.println("Invalid input!");
                }
                else {
                   String firstSwap = matrix[rowsFirst][colsFirst];
                   String secondSwap = matrix[rowsSecond][colsSecond];

                   matrix[rowsSecond][colsSecond] = firstSwap;
                   matrix[rowsFirst][colsFirst] = secondSwap;

                    for (String[] strings : matrix) {
                        for (String string : strings) {
                            System.out.print(string + " ");
                        }
                        System.out.println();
                    }
                }
            }
            else {
                System.out.println("Invalid input!");
            }


            scan = scanner.nextLine();
        }




    }
}