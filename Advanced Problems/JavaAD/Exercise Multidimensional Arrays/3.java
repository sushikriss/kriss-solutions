package com.company; // USE COLLECTIONS MORE OFTEN :)
import java.util.*;
public class Kriss {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int size = Integer.parseInt(scanner.nextLine());

        int[][] matrix = new int[size][size];

        for (int i = 0; i < size; i++) {
            int[] arr = Arrays.stream(scanner.nextLine()
            .split(" "))
                    .mapToInt(Integer::parseInt)
                    .toArray();
            matrix[i] = arr;
        }

        int counter = 0;
        int secondCounter = 0;
        int sumFirst = 0;
        for (int i = 0; i < matrix.length; i++) {

                sumFirst += matrix[counter][secondCounter];
                counter++;
                secondCounter++;

        }

        counter = matrix.length - 1;
        secondCounter = 0;
        int sumSecond = 0;
        for (int i = 0; i < matrix.length; i++) {

            sumSecond += matrix[counter][secondCounter];
            counter--;
            secondCounter++;
        }

        System.out.println(Math.abs(sumFirst - sumSecond));


    }
}