package com.company; // USE COLLECTIONS MORE OFTEN :)
import java.util.*;
public class Kriss {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] input = scanner.nextLine().split(" ");
        int rows = Integer.parseInt(input[0]);
        int cols = Integer.parseInt(input[1]);

        char[][] matrix = new char[rows][cols];
        char ch = 'a';

        for (int i = 0; i < rows; i++) {

            for (int j = 0; j < cols; j++) {

                System.out.print(ch);
                System.out.print((char) (ch + j));
                System.out.print(ch);
                System.out.print(" ");
            }
            System.out.println();
            ch++;
        }


    }
}