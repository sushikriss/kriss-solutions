package com.company;

public class StackNoElementsException extends Exception {

    public StackNoElementsException(String message) {
        super(message);
    }
}