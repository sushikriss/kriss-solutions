package com.company;

; // USE COLLECTIONS MORE OFTEN :) // read carefully c:

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        List<Integer> nums = Arrays.stream(scanner.nextLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());
        int division = Integer.parseInt(scanner.nextLine());

        Predicate<Integer> isDivisible = num -> num % division != 0;
        Function<Integer, Boolean> filterNums = isDivisible::test;

        Collections.reverse(nums);
        nums.stream().filter(filterNums::apply).forEach(z -> System.out.printf("%d ", z));
    }
}
