package com.company; // USE COLLECTIONS MORE OFTEN :) // read carefully c:
import java.util.*;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());

        LinkedHashMap<String, Car> cars = new LinkedHashMap<>();
        while (n-- > 0) {
            String[] carInfo = scanner.nextLine().split("\\s+");

            String model = carInfo[0];
            int engineSpeed = Integer.parseInt(carInfo[1]);
            int enginePower = Integer.parseInt(carInfo[2]);
            int cargoWeight = Integer.parseInt(carInfo[3]);
            String cargoType = carInfo[4];
            double tier1 = Double.parseDouble(carInfo[5]);
            double tier1o = Double.parseDouble(carInfo[6]);
            double tier2 = Double.parseDouble(carInfo[7]);
            double tier2o = Double.parseDouble(carInfo[8]);
            double tier3 = Double.parseDouble(carInfo[9]);
            double tier3o = Double.parseDouble(carInfo[10]);
            double tier4 = Double.parseDouble(carInfo[11]);
            double tier4o = Double.parseDouble(carInfo[12]);

            cars.put(model, new Car(model, engineSpeed, enginePower, cargoWeight,
                    cargoType, tier1, tier1o, tier2, tier2o, tier3, tier3o,
                    tier4, tier4o));
        }

        String typeCargo = scanner.nextLine();

        if (typeCargo.equals("fragile")) {
            for (Map.Entry<String, Car> s : cars.entrySet()) {
                if (s.getValue().getCargoType().equals("fragile") &&
                s.getValue().getTier1Pressure() < 1 ||
                        s.getValue().getTier2Pressure() < 1 ||
                        s.getValue().getTier3Pressure() < 1 ||
                        s.getValue().getTier4Pressure() < 1) {
                    System.out.println(s.getValue().getModel());
                }
            }
        } else {
            for (Map.Entry<String, Car> s : cars.entrySet()) {
                if (s.getValue().getCargoType().equals("flamable") &&
                        s.getValue().getEnginePower() > 250 ) {
                    System.out.println(s.getValue().getModel());
                }
            }
        }
    }
}
