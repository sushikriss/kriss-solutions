package com.company;
public class Car {
    private String model;
    private int engineSpeed;
    private int enginePower;
    private int cargoWeight;
    private String cargoType;

    private double tier1Pressure;
    private double tier1Age;

    private double tier2Pressure;
    private double tier2Age;

    private double tier3Pressure;
    private double tier3Age;

    private double tier4Pressure;
    private double tier4Age;

    public String getCargoType() {
        return cargoType;
    }

    public void setCargoType(String cargoType) {
        this.cargoType = cargoType;
    }

    public Car(String model, int engineSpeed, int enginePower,
               int cargoWeight, String cargoType, double tier1,
               double tier1o, double tier2, double tier2o, double tier3,
               double tier3o, double tier4, double tier4o) {
        this.model = model;
        this.engineSpeed = engineSpeed;
        this.enginePower = enginePower;
        this.cargoWeight = cargoWeight;
        this.cargoType = cargoType;
        this.tier1Pressure = tier1;
        this.tier1Age = tier1o;
        this.tier2Pressure = tier2;
        this.tier2Age = tier2o;
        this.tier3Pressure = tier3;
        this.tier3Age = tier3o;
        this.tier4Pressure = tier4;
        this.tier4Age = tier4o;
    }


    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getEngineSpeed() {
        return engineSpeed;
    }

    public void setEngineSpeed(int engineSpeed) {
        this.engineSpeed = engineSpeed;
    }

    public int getEnginePower() {
        return enginePower;
    }

    public void setEnginePower(int enginePower) {
        this.enginePower = enginePower;
    }

    public double getTier1Pressure() {
        return tier1Pressure;
    }

    public void setTier1Pressure(double tier1Pressure) {
        this.tier1Pressure = tier1Pressure;
    }

    public double getTier1Age() {
        return tier1Age;
    }

    public void setTier1Age(double tier1Age) {
        this.tier1Age = tier1Age;
    }

    public double getTier2Pressure() {
        return tier2Pressure;
    }

    public void setTier2Pressure(double tier2Pressure) {
        this.tier2Pressure = tier2Pressure;
    }

    public double getTier2Age() {
        return tier2Age;
    }

    public void setTier2Age(double tier2Age) {
        this.tier2Age = tier2Age;
    }

    public double getTier3Pressure() {
        return tier3Pressure;
    }

    public void setTier3Pressure(double tier3Pressure) {
        this.tier3Pressure = tier3Pressure;
    }

    public double getTier3Age() {
        return tier3Age;
    }

    public int getCargoWeight() {
        return cargoWeight;
    }

    public void setCargoWeight(int cargoWeight) {
        this.cargoWeight = cargoWeight;
    }

    public void setTier3Age(double tier3Age) {
        this.tier3Age = tier3Age;
    }

    public double getTier4Pressure() {
        return tier4Pressure;
    }

    public void setTier4Pressure(double tier4Pressure) {
        this.tier4Pressure = tier4Pressure;
    }

    public double getTier4Age() {
        return tier4Age;
    }

    public void setTier4Age(double tier4Age) {
        this.tier4Age = tier4Age;
    }
}
