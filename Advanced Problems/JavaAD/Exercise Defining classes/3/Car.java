package com.company;

public class Car {
    private String model;
    private double fuelAmount;
    private double costPerKilo;
    private int distanceTravelled = 0;

    public Car(String model, double fuelAvailable, double costPerKilometer) {
        this.model = model;
        this.fuelAmount = fuelAvailable;
        this.costPerKilo = costPerKilometer;
    }

    public String getModel() {
        return model;
    }

    public double getFuelAmount() {
        return fuelAmount;
    }

    public int getDistanceTravelled() {
        return distanceTravelled;
    }

    boolean fuelUp(int distance) {
        double total = distance * this.costPerKilo;
        if (this.fuelAmount - total >= 0) {
            this.fuelAmount -= total;
            this.distanceTravelled += distance;
            return true;
        } else {
            return false;
        }
    }

}
