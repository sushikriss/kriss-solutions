package com.company; // USE COLLECTIONS MORE OFTEN :) // read carefully c:
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());

        LinkedHashMap<String, Car> cars = new LinkedHashMap<>();

        while (n-- > 0) {
            String[] input = scanner.nextLine().split("\\s+");

            String model = input[0];
            double fuelAvailable = Double.parseDouble(input[1]);
            double costPerKilometer = Double.parseDouble(input[2]);

            cars.put(model, new Car(model, fuelAvailable, costPerKilometer));
        }

        String input = scanner.nextLine();
        while (!input.equals("End")) {
            String[] info = input.split("\\s+");

            String carModel = info[1];
            int kms = Integer.parseInt(info[2]);

            if (!cars.get(carModel).fuelUp(kms)) {
                System.out.println("Insufficient fuel for the drive");
            }

            input = scanner.nextLine();
        }

        for (Map.Entry<String, Car> s : cars.entrySet()) {
            System.out.println(s.getValue().getModel() + " " +
                   String.format("%.2f", s.getValue().getFuelAmount()) + " " + s.getValue().getDistanceTravelled());
        }
    }
}
