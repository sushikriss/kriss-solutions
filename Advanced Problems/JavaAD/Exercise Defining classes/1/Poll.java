package com.company;
public class Poll {
    private String name;
    private int age;

    public void getActualAge(int age) {
        this.age = age;
    }

    public void getActualName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

}
