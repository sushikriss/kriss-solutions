package com.company; // USE COLLECTIONS MORE OFTEN :) // read carefully c:
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int countOfPeople = Integer.parseInt(scanner.nextLine());
        Map<String, Integer> peoplesAge = new TreeMap<>();

        while (countOfPeople-- > 0) {
            String[] arr = scanner.nextLine().split("\\s+");

            String name = arr[0];
            int age = Integer.parseInt(arr[1]);

            Poll person = new Poll();
            person.getActualName(name);
            person.getActualAge(age);

            if (age > 30) {
                peoplesAge.put(person.getName(), person.getAge());
            }
        }

        for (Map.Entry<String, Integer> personEntry : peoplesAge.entrySet()) {
            System.out.println(personEntry.getKey() + " - " + personEntry.getValue());
        }

    }
}
